<?php
/*
Template Name: Offer - Subpage-full
Do not use!
*/

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<div id="main" class="main single-offer full-width" role="main">
  <div class="page-header">
    <a href="<?php echo get_permalink($post->post_parent); ?>" class="parent-link"><?php the_field('text_back', 'option'); ?></a>
    <hgroup>
      <h1><?php the_title(); ?></h1>
      <h2><?php the_field( 'the_second_line_of_the_title_in_black' ); ?></h2>
    </hgroup>

  </div>
  <img class="detailimage" style="float: left; margin-right: 10px;" src="<?php the_field('detail_image'); ?>" />
  <div class="main-text">
    <?php the_content(); ?>
  </div>

  <div class="main-text">

    <?php if(get_field('offer_features')): ?>

      <ul>

      <?php while(has_sub_field('offer_features')): ?>

        <div class="offer-feature has-image">
				<?php
				$image = get_sub_field('feature_image');
				if( !empty($image) ): ?>
					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" title="<?php echo $image['title']; ?>" />
				<?php endif; ?>

          <div class="text-content">
            <h3><?php the_sub_field('feature_title'); ?></h3>
            <?php the_sub_field('feature_detail'); ?>

            <?php // Add Button
            if( get_sub_field('has_button') ) { ?>
              <a href="<?php the_sub_field('button_url'); ?>" target="<?php the_sub_field('button_target'); ?>" class="button getattr"><?php the_sub_field('button_text'); ?></a>
            <?php } ?>
          </div>
        </div>

      <?php endwhile; ?>

      </ul>

    <?php endif; ?>

  </div>
	<div id="nya" class="nya">
		<p class="nya-text"><?php the_field( 'book_now_in_sub-offer_pages_pop_up', 'option' ); ?></p>
	</div>
</div>
<!-- #main -->

<?php
endwhile;


get_footer();
