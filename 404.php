<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package amstelhouse
 */

get_header(); ?>

<div id="headerwrap">
			<div class="booknow">
				<img src="<?php echo get_template_directory_uri(); ?>/img/bookhere.gif" alt="book here" title="book">
			</div>
</div>

	<div id="main" class="generic" role="main">
		<div class="container-fluid fullheightarea Aligner">
			<div class="row">
				<div class="col-xs-12 col-md-10 col-md-offset-1">
	
					<div class="main-text">
					<h1>Excuse me, this page does not exist.</h1>
							
		
		
					</div>
	
				</div>
			</div>
		</div>
	</div>


<?php

get_footer();
