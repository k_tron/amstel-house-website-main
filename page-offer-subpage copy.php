<?php
/*
Template Name: Offer - Subpage_test
*/
get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
<div id="headerwrap">
<div class="booknow">
				<img src="<?php echo get_template_directory_uri(); ?>/img/bookhere.png">
			</div>
<?php
$img_src = wp_get_attachment_image_url( get_post_thumbnail_id( $post->ID ), 'full' );
$img_srcset = wp_get_attachment_image_srcset( get_post_thumbnail_id( $post->ID ), 'full' );
$img_alt = get_post_meta( get_post_thumbnail_id( $post->ID ), '_wp_attachment_image_alt', true);
?>
<img class=" mobilesmaller fillwidth fullheightarea" src="<?php echo esc_url( $img_src ); ?>"
     srcset="<?php echo esc_attr( $img_srcset ); ?>"
     sizes="(max-width: 1024px) 100vw, 1024w" alt="<?php echo $img_alt; ?>">
<h1 class="header-title"><?php the_title(); ?></h1>
			<p class="hostel-adrress"><?php the_field('line_below_slider', 'option'); ?></p>
	</div>
	<div id="main" class=" homepage " role="main">
		<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-10 col-md-offset-1">
				<div class="welcome">
					<div class="welcome-body span4 collapse-group">
						 <div class="intro">
      <?php the_content(); ?>
    </div>
					</div>
				</div>
			</div>
		</div>
	</div>
<section>
	<div class="container-fluid se-container roomspage2 offer-feature">
                    <div class="row">
                        <div class="col-xs-12 col-md-10 col-md-offset-1">
		      <?php
			       $room_i = 1;
			       while(has_sub_field('offer_features')):
		      ?>
		       <a class="fancybox" href="<?php the_sub_field('feature_image'); ?>" title="<?php echo $room_img1_title; ?>" data-fancybox-group="room-<?php echo $room_i; ?>">
              <img class="fullwidthimg" src="<?php the_sub_field('feature_image'); ?>" />
            </a>
		      <div class="room-type">
        <div class="text-content">
            <h3><?php the_sub_field('feature_title'); ?></h3>
	          <div class="teamtext ">
		          <br />
		          <?php the_sub_field('feature_detail'); ?>
	          </div>
	         <?php if( get_sub_field('has_button') ) { ?>
	         <div class="book-room-now" >
                <a class="button getattr" href="<?php the_sub_field('button_url'); ?>">
                    <?php the_sub_field('button_text'); ?>
                </a>
            </div>
            <?php } ?>
         <?php $room_i++; endwhile; ?>
        </div>
   	</div><!--/row-->
  </div>
</section>
<!--
	<div id="nya" class="nya">
		<p class="nya-text"><?php the_field( 'book_now_in_sub-offer_pages_pop_up', 'option' ); ?></p>
	</div>
-->
<!-- #main -->
<?php
endwhile;
get_footer();
