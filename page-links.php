<?php
/*
Template Name: Links Page
*/

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>


<div id="headerwrap">
			<div class="booknow">
				<img src="<?php echo get_template_directory_uri(); ?>/img/bookhere.gif" alt="book here" title="book">
			</div>

</div>
	<div id="main" class="generic" role="main">
		<div class="container-fluid fullheightarea Aligner">
			<div class="row">
				<div class="col-xs-12 col-md-10 col-md-offset-1">
	<h1 class="header-title"><?php the_title(); ?></h1>
					<div class="main-text">
					<?php the_content(); ?>
							<?php if( get_field( 'link_list_links' ) ): ?>
			
			<h4>Links</h4>
			<ul class="links">
			<?php while( has_sub_field( 'link_list_links' ) ): ?>
				<li><a target="<?php the_sub_field( 'link_target' ); ?>" href="<?php the_sub_field( 'link_url' ); ?>" target="_blank"><?php the_sub_field( 'link_name' ); ?></a></li>
			<?php endwhile; ?>
			</ul>
		
		<?php endif; ?>
		
		<?php if( get_field( 'link_list_backpacker' ) ): ?>
		
			<div class="backpacker-network-list">
				<h4>Backpacker Network</h4>
				<ul class="links">
				<?php while(has_sub_field( 'link_list_backpacker' ) ): ?>
				<li><a target="<?php the_sub_field( 'link_target' ); ?>" href="<?php the_sub_field( 'link_url' ); ?>" target="_blank"><?php the_sub_field( 'link_name' ); ?></a></li>
				<?php endwhile; ?>		
				</ul>
			</div>
		
		<?php endif; ?>
					</div>
	
				</div>
			</div>
		</div>
	</div>
	
	<?php endwhile; // end of the loop. ?>

<?php 
get_footer();
