<?php
/*
Template Name: Services Page
*/

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>


<div id="headerwrap">
<div class="booknow">
				<img src="<?php echo get_template_directory_uri(); ?>/img/bookhere.gif" alt="book here" title="book">
			</div>
<?php
$img_src = wp_get_attachment_image_url( get_post_thumbnail_id( $post->ID ), 'full' );
$img_srcset = wp_get_attachment_image_srcset( get_post_thumbnail_id( $post->ID ), 'full' );
$img_alt = get_post_meta( get_post_thumbnail_id( $post->ID ), '_wp_attachment_image_alt', true);
?>
<img class=" mobilesmaller fillwidth fullheightarea" src="<?php echo esc_url( $img_src ); ?>"
     srcset="<?php echo esc_attr( $img_srcset ); ?>"
     sizes="(max-width: 1024px) 100vw, 1024w" alt="<?php echo $img_alt; ?>">

<div class="header-title"><?php the_title(); ?></div>
<?php
					$link_f = get_field( 'main_image_link', 'option' );
					if ( $link_f ) {
						$link = $link_f;
					} else {
						$link = '/best-price-guarantee';
					}
 ?>	 <a href="<?php echo esc_url( $link ); ?>">
    <div class="hidden-xs">
        <p class="hostel-adrress"><?php the_field('line_below_slider', 'option'); ?></p>
    </div>

    <div class="visible-xs-block">
        <p class="hostel-adrress"><?php the_field('mobile_line_below_slider', 'option'); ?></p>
    </div></a>
	</div>

	<div id="main" class="location" role="main">
		<div class="container fullheightarea Aligner hidden-xs">
		<div class="row">
			<div class="col-xs-12 col-md-8 col-md-offset-2">
				<div class="welcome">
					<div class="welcome-body span4 collapse-group">
						<div class="intro">
							<?php the_content(); ?>
    					</div>

					</div>
				</div>
			</div>
		</div>
	</div>



<section id="servicespage">
	<div class="se-content container-fluid  fullheightarea Aligner">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-lg-10 col-lg-offset-1">
                <div class="Grid" style="  max-width: 100%; max-height: 100%;">
                    <?php
	                     while (has_sub_field('service_list')): ?>

                    <div class=" Grid-item four <?php the_sub_field('image_position'); ?>">
                        <div
                        class="badgeb left" style="background-image: url('<?php the_sub_field('service_image'); ?>');">
												<div class="hiddenholder hidden-lg hidden-sm hidden-md">
                            <span><h3><?php the_sub_field('service_title'); ?></h3>
                            <?php the_sub_field('service_detail'); ?>
													</span>
                        </div>
												</div>
                    </div>
										<div class="hidden-xs Grid-item four <?php the_sub_field('image_position'); ?>">
                        <div
                        class="badgeb left servicetext"  >
                        <div class="hiddenholder ">
                            <span><h3><?php the_sub_field('service_title'); ?></h3>
                            <?php the_sub_field('service_detail'); ?></span>
                        </div>
                        </div>
                    </div>

									<?php endwhile; ?>
                </div>
            </div>
        </div>
	</div>
</section>
<!-- #main -->
	</div>
<?php
endwhile;
get_footer();
