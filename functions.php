<?php

ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
error_reporting(E_ALL ^ E_NOTICE);
/**
 * Amstelhouse functions and definitions
 *
 * @package Amstelhouse
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */

  $version = '2.4';
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}


//remove_filter ('the_content', 'wpautop');

if ( ! function_exists( 'amstelhouse_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function amstelhouse_setup() {

		/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Amstelhouse, use a find and replace
	 * to change 'amstelhouse' to the name of your theme in all the template files
	 */
		load_theme_textdomain( 'amstelhouse', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'post-thumbnails' );

		/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
		add_theme_support( 'title-tag' );

		/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
		//add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
				'header-menu' => __( 'Header Menu', 'amstelhouse' ),
			) );

		/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
		add_theme_support( 'html5', array(
				'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
			) );

		/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
		add_theme_support( 'post-formats', array(
				'aside', 'image', 'video', 'quote', 'link',
			) );

	}
endif; // amstelhouse_setup
add_action( 'after_setup_theme', 'amstelhouse_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
// function amstelhouse_widgets_init() {
// 	register_sidebar( array(
// 			'name'          => __( 'Sidebar', 'amstelhouse' ),
// 			'id'            => 'sidebar',
// 			'description'   => '',
// 			'before_widget' => '<div id="%1$s" class="teaser widget %2$s"><div class="box-inner">',
// 			'after_widget'  => '</div></div>',
// 			'before_title'  => '<h2 class="widget-title">',
// 			'after_title'   => '</h2>',
// 		) );
// }
// add_action( 'widgets_init', 'amstelhouse_widgets_init' );
//

// add to your theme functions.php
function royalslider_change_image_size($sizes) {
    $sizes['large'] = 'full';
    return $sizes;
}
add_filter( 'new_rs_image_sizes', 'royalslider_change_image_size' );

/**
 * Enqueue scripts and styles.
 */

function bootstrap_scripts() {
  global $version;
  //wp_enqueue_script( 'jquery-ui', 'https://code.jquery.com/ui/1.12.1/jquery-ui.min.js', array( 'jquery' ), true );

  // wp_enqueue_script( 'touchswipe', '//cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.4/jquery.touchSwipe.js', array( 'jquery' ) );
  wp_enqueue_script( 'custom-script', get_template_directory_uri() . '/js/theme.min.js?v=36', array( 'jquery' ), '1.0.0', true);
  // wp_enqueue_style( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css', false, '3.2.0');
  wp_enqueue_style( 'googleicons', 'https://fonts.googleapis.com/icon?family=Material+Icons', false , null );
  wp_enqueue_style( 'amstelhouse-style', get_stylesheet_uri(), false, '3');
  wp_enqueue_style( 'stylesheet', get_template_directory_uri() . '/css/theme.min.css?v=5.5', array(), '2');
//  wp_enqueue_style( 'fontsawesomebrands', 'https://use.fontawesome.com/releases/v5.6.3/css/brands.css', false, '2.2.1');
  wp_enqueue_style( 'fontsawesome', 'https://use.fontawesome.com/releases/v5.6.3/css/all.css', false, '2.2.1');

}

add_action( 'wp_enqueue_scripts', 'bootstrap_scripts' );

if( is_home() || is_front_page() ) :

 endif;


 function build_js(){
     if( is_single() && get_post_type()=='rooms' ){
         register_new_royalslider_files(7);
     }
 }
 add_action('wp_enqueue_scripts', 'build_js');


function shapeSpace_include_custom_jquery() {

	wp_deregister_script('jquery');
	wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js', array(), "3.3.1", false);

}
add_action('wp_enqueue_scripts', 'shapeSpace_include_custom_jquery');
add_action( 'wp_enqueue_scripts', 'enqueue_royal_sliders' );
function enqueue_royal_sliders() {
    global $posts;

    // can be also is_archive(), is_page() e.t.c.
    if(is_page_template( 'page-offer-subpage-about.php' )) { 
        register_new_royalslider_files(2); 
    }
}

add_filter( 'wpcf7_load_js', '__return_false' ); // Disable CF7 JavaScript
add_filter( 'wpcf7_load_css', '__return_false' ); // Disable CF7 CSS
add_action('wp_enqueue_scripts', 'load_wpcf7_scripts');
function load_wpcf7_scripts() {
    if ( is_page_template( 'page-offer-groups.php' ) ) {
        if ( function_exists( 'wpcf7_enqueue_scripts' ) ) {
            wpcf7_enqueue_scripts();
        }
        if ( function_exists( 'wpcf7_enqueue_styles' ) ) {
            wpcf7_enqueue_styles();
        }
    }
}
// if(!is_admin()) {
//   add_filter( 'script_loader_tag', function ( $tag, $handle ) {
//     return str_replace( ' src', ' async src', $tag );
//   }, 10, 2 );
// }

if (! function_exists('my_prefix_fetch_data')) {
    function my_prefix_fetch_data($query)
    {
        $cache_time = (! empty($cache_time)) ? absint($cache_time) : DAY_IN_SECONDS;
        $cache_key = md5(__CLASS__ . 'fetch_data');
        if (0 < $cache_time && false != ($cached = get_transient($cache_key))) {

            return  $cached;
           // return;
        }

        $response = wp_safe_remote_get('https://www.amstelhouse.de/blog/wp-json/wp/v2/' . $query);
        if (! is_wp_error($response)) {
            $output = json_decode(wp_remote_retrieve_body($response));

            if (empty($output)) {
                return array();
            }
            if (0 < $cache_time) {
                set_transient($cache_key, $output, $cache_time);
            } else {
                delete_transient($cache_key);
            }


            return $output;
        }
    }
}


/*Function to defer or asynchronously load scripts*/

// function js_async_attr($tag){
//
// # Do not add defer or async attribute to these scripts
// $scripts_to_exclude = array('https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js', 'https://www.amstelhouse.de/wp-content/plugins/contact-form-7/includes/js/scripts.js');
//
// foreach($scripts_to_exclude as $exclude_script){
//  if(true == strpos($tag, $exclude_script ) )
//  return $tag;
// }
//
// # Defer or async all remaining scripts not excluded above
// return str_replace( ' src', ' defer="async" src', $tag );
// //return str_replace( ' src', ' defer="defer" src', $tag );
// }
//
// if(!is_admin()) {
//   add_filter( 'script_loader_tag', 'js_async_attr', 10 );
//   // add_filter( 'script_loader_tag', function ( $tag, $handle ) {
//   //   return str_replace( ' src', ' async src', $tag );
//   // }, 10, 2 );
// }


// stop wp removing div tags
function magnium_tinymce_fix( $init )
{
    // html elements being stripped
    $init['extended_valid_elements'] = 'div[*], article[*]';
    // don't remove line breaks
    $init['remove_linebreaks'] = false;
    // convert newline characters to BR
  //  $init['convert_newlines_to_brs'] = true;
    // don't remove redundant BR
    $init['remove_redundant_brs'] = false;
    // pass back to wordpress
    return $init;
}
add_filter('tiny_mce_before_init', 'magnium_tinymce_fix');



function awesome_acf_responsive_image($image_id,$image_size,$max_width){

	// check the image ID is not blank
	if($image_id != '') {

		// set the default src image size
		$image_src = wp_get_attachment_image_url( $image_id, $image_size );

		// set the srcset with various image sizes
		$image_srcset = wp_get_attachment_image_srcset( $image_id, $image_size );

		// generate the markup for the responsive image
		echo 'src="'.$image_src.'" srcset="'.$image_srcset.'" sizes="(max-width: '.$max_width.') 100vw, '.$max_width.'"';

	}
}


function newrs_add_custom_alt_variable($m, $data, $options) {
    $m->addHelper('alt_img', function() use ($data) {
        // $data object holds all data about slide
        if(isset($data['image']) && isset($data['image']['attachment_id'])) {
            $attachment_id = $data['image']['attachment_id'];
            return wp_get_attachment_image_srcset($attachment_id, 'large');
          //  return get_post_meta($attachment_id, '_wp_attachment_image_alt', true);
         }
         return '';
    } );
}
add_filter('new_rs_slides_renderer_helper','newrs_add_custom_alt_variable', 10, 4);





/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';


/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

//require get_template_directory() . '/inc/define-widgets.php';

require get_template_directory() . '/inc/shin-perth.php';
