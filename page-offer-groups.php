<?php
/*
Template Name: Offer - Students
*/


get_header(); ?>
<style type="text/css">
  .yellowbox.groupbox {
    background: #faf03d;
  }

  .yellowbg {
    background: #f9ee50;
    position: relative;
  }

  .group section.yellowbox.se-container.hidden-xs.base {
    /*  padding-bottom:  0px;
  margin-bottom:  0px;*/
  }

  .group .yellowbox .text-content h3 {
    background: transparent;
    color: black;
    line-height: 1.3em;
    padding-top: 20px !important;
    padding-bottom: 0px !important;
    font-family: Phosphate-s,arial,sans-serif;
  }


  .group section.mainskew.hasimageina.bgcover.hidden-xs {
    clip-path: polygon(0px 15vw, 100% 0px, 100% 171vw, 0% 100%);
    padding-top: 15vw;
    padding-bottom: 15vw;
    margin-bottom: -16vw;
  }

  .group .textskew {}

  section.hstop {
    /*   margin-top: calc(-1 *100vw * 0.1227845609); */
    /* background: white; */
    /* z-index: 1; */
    /* position: relative; */
    z-index: 1;
    position: relative;
  }

  .col-xs-12.col-md-10.col-md-offset-1.sixtyup {
    margin-top: -80px;
  }

  .blacktitle {
    background-color: #000000;
    color: #fff;
    /* display: inline-block; */
    font-size: 1.2em;
    font-weight: bold;
    line-height: 1.2em;
    /* margin: 20px 0 0; */
    padding: 1vh;
    text-transform: uppercase;
    width: auto;
    text-align: center;
    /* margin-top: 35px; */
    font-weight: bold;
    font-family: "proxima_nova_bold", helvetica, ariel, sans-serif;
    text-transform: uppercase;
    letter-spacing: 0.03em;
  }

  .yellowbox.groupbox {
    padding: 20px;
  }

  .group .img-circle {

    background: #ddd;
    line-height: 0em;
    padding: 25px;
  }

  .group .panel-body.room-type p {
    text-align: center;
    margin-top: auto;
	width: 100%;

  }




  .group .room-type {
    width: 100%;
  }

  .group .img-circle img {
    text-align: center;
    margin: 0 auto;
    /* background: #ddd; */
  }

  .bluecontact h3 {
    display: block;
    width: 100%;
    text-align: center;
    padding-top: 0px;
  }

  h3 {}

  .bluecontact {
    background: #184d9f;
    color: white;
    padding: 40px;
  }

  .bluecontact>p {
    padding: 10px 0px;
  }

  span.wpcf7-list-item.first {
    /* display: inline-block; */
  }

  span.wpcf7-form-control.wpcf7-checkbox.radiobutt {
    display: flex;
    display: flex;
    justify-content: space-between;
    padding: 10px 0px;
  }

  p.full-width-flex {
    flex: 1 0 100%;
    justify-content: center;
    align-self: center;
    align-items: center;
  }




  hr.groupdivider {
    width: 66.7%;
    margin: 35px 0px;
    /* display: block; */
    margin-right: 0px;
    margin-left: auto;
  }

  .row.sm-icon-row>div {
    /*   padding: 5px; */
  }

  .sm-iconholder {
    display: flex;
    margin: 5px 0px;
    justify-content: flex-end;
  }

  .sm-icon-row .fa {
    margin-left: 10px;
    display: inline-flex;
    align-self: center;
  }

  .sm-icon-row em {
    display: inline-flex;
    /* word-wrap: break-word; */
    line-height: 1.3em;
  }


  .room-typea {
    margin: 20px 0px 10px 0px;
  }

  .bgcover {
    background-size: cover;
  }

  div#ui-datepicker-div {
    background: whitesmoke;
  }

  @media screen and (max-width: 768px) {


    .bluecontact {
      padding: 15px;
    }

    .group .img-circle {
      /* max-width: initial; */
    }

    .group .room-type {
      padding: 30px 0 40px 0;
    }

    .group .panel {
      margin-bottom: 0px;

    }

    .group .offsetskew {
      height: 50em;
    }

    .col-md-6.col-xs-12 {
	margin-bottom: 15px;
}

.room-type.card-body {
	border-bottom: none !important;
	margin-bottom: 0px;
	padding-bottom: 0px;
}

h3.text-uppercase.sectiontitle {
	font-size: 20px;
	padding-bottom: 0px;
}

.row.evenheight {
	margin: 0px !important;
}


section.yellowbg.offerspadder {
	padding-top: 0px;
}


  }

  .roomspage ul {
    text-align: left;
    /* list-style: circle; */
    margin-left: 15px;
    list-style-type: disc;
    /* margin-bottom: 30px; */
    columns: 2;
    -webkit-columns: 2;
    -moz-columns: 2;
  }

  .roomspage .text-content p {
    margin-bottom: 5%;
    text-align: left;
    margin-top: 5%;
  }
  .roomspage em {
    font-size: 1.2em;
	font-family: proxima_nova_bold,helvetica,ariel,sans-serif;
	text-transform: uppercase;
	letter-spacing: .03em;
  }
  .roomspage h4 {
    font-family: Phosphate-s,arial,sans-serif;
	letter-spacing: .05em;
	line-height: 1.3em;
	COLOR: black;
	font-size: 2em !important;
	margin: 0px;
  font-weight: 500;

  }
  .card-body {
	background: white;

}
.roomspage
.panel.panel-default {
	/* border: 3px solid black; */
	border: 2px solid #000;
  background: white;
	display: flex;
	flex-wrap: wrap;

}
.roomspage .col-md-6.col-xs-12 {
	display: flex;
	/* background: white; */
	/* height: 100%; */
}
.room-type .iconrow {
	flex-wrap: wrap;
	justify-items: initial;
	justify-content: flex-start;
	max-height: 100%;
}

</style>

<?php while (have_posts()) : the_post(); ?>

<div id="headerwrap">
  <div class="booknow">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/bookhere.gif?v=5" alt="book here" title="book"
      width="120" srcset="https://www.amstelhouse.de/wp-content/themes/amstelhouse-staging/img/bookhere_x2.gif 2x">
  </div>
  <?php
$img_src = wp_get_attachment_image_url(get_post_thumbnail_id($post->ID), 'full');
$img_srcset = wp_get_attachment_image_srcset(get_post_thumbnail_id($post->ID), 'full');
$img_alt = get_post_meta(get_post_thumbnail_id($post->ID), '_wp_attachment_image_alt', true);
?>
  <img class=" mobilesmaller fillwidth fullheightarea" src="<?php echo esc_url($img_src); ?>"
    srcset="<?php echo esc_attr($img_srcset); ?>" sizes="(max-width: 1024px) 100vw, 1024w"
    alt="<?php echo $img_alt; ?>">
  <div class="header-title"><?php the_title(); ?></div>
  <?php
                    $link_f = get_field('main_image_link', 'option');
                    if ($link_f) {
                        $link = $link_f;
                    } else {
                        $link = '/best-price-guarantee';
                    }
 ?>
  <!-- <a href="<?php echo esc_url($link); ?>"> -->
  <a href="https://www.amstelhouse.de/<?php echo $language ?>hygiene/">
    <div class="hidden-xs">
      <p class="hostel-adrress"><?php the_field('line_below_slider', 'option'); ?></p>
    </div>

    <div class="visible-xs-block">
      <p class="hostel-adrress"><?php the_field('mobile_line_below_slider', 'option'); ?></p>
    </div>
  </a>

</div>

<div id="main" class="group offerspage" role="main">
  <div class="mobilesmaller container-fluid fullheightarea Aligner">
    <div class="row">
      <div class="col-xs-12 col-md-8 col-md-offset-2">
        <div class="welcome">
          <div class="welcome-body span4 collapse-group">
            <div class="intro">
              <?php the_content(); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <section class="mainskew yellowbox base">
    <div class="right12margin center-block textskew">
      <h2></h2>
      </div>
    <div class="container pad80 fullheightarea Aligner">
      <div class="row auto-clear">
        <?php 
         while (has_sub_field('3_cards')): ?>

        <div class="col-sm-4 col-xs-12">
          <div class="panel panel-default">
            <div class="panel-thumbnail">
              <?php
            $image = get_sub_field('image');
            if (!empty($image)): ?>
              <img class="img-responsive fullwidthimg" src="<?php echo $image['url']; ?>"
                alt="<?php echo $image['alt']; ?>" title="<?php echo $image['title']; ?>" />
              <?php endif; ?>

            </div>
            <div class="panel-body room-type">
              <div class="text-content">
                <h3><?php the_sub_field('title'); ?></h3>
                <div class="pad25 text-left" style="
                 font-weight: 700;
                font-size: 1.2em;"><?php the_sub_field('detail'); ?></div>
              </div>

            </div>
          </div>
          <!--/panel-->
        </div>
        <!--/col-->

        <?php endwhile; ?>


      </div>
      <!--/row-->
    </div>
  </section>

  <section class="mainskew hasimageina bgcover "
    style="background-image: url(<?php the_field('bottom_section_image'); ?>);   margin-bottom: 0px;">
    <div class="right12margin center-block textskew yellowbox imagebox-y">
      <h2><?php the_field('second_section_title'); ?></h2>
    </div>
    <div class="se-container se-slope up iconsbox imgspacer offsetskewa fullheightarea Aligner ">
    </div>
  </section>
  <section class="hstop yellowbg skewmarginbottom">
    <div class="se-content container-fluid">
      <div class="row" style="  align-self: start;">
        <div class="col-xs-12 col-md-10 col-md-offset-1 sixtyup">
          <div class="row ">
            <div class="col-xs-12">
              <?php if (get_field('4_icons')): $i = 0; while (has_sub_field('4_icons')): $i++;  ?>
              <div class="col-sm-3 col-xs-12 mobile-margin-top">

                <div class="img-circle">
                  <?php
                      $image = get_sub_field('image');
                      if (!empty($image)): ?>
                  <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"
                    title="<?php echo $image['title']; ?>" />
                  <?php endif; ?>
                </div>
                <div class="room-typea">
                  <div class="text-content">
                    <h5><?php the_sub_field('title'); ?></h5>
                  </div>
                </div>

              </div>
              <?php  endwhile; endif; ?>
            </div>

          </div>
          <!-- <div class="row">
            <div class="col-xs-12 col-md-8 col-md-offset-2 pad80 ">
            </div>

          </div> -->
        </div>
      </div>
    </div>
  </section>
  <section class="yellowbg offerspadder">
    <div class="text-center center-block">
    
        <h3 class="text-uppercase sectiontitle"><?php the_field('rooms_title'); ?></h3>

      </div>
    <div class="container Aligner roomspage pad80">
      
      <div class="row">
        <div class="col-xs-12 col-md-10 col-md-offset-1 ">
          <div class="row evenheight">
            <?php 
         while (has_sub_field('student_rooms')): ?>

            <div class="col-md-6 col-xs-12">
              <div class="panel panel-default">
                <div class="panel-thumbnail">
                  <?php
            $image = get_sub_field('image');
            if (!empty($image)): ?>
                  <img class="img-responsive fullwidthimg" src="<?php echo $image['url']; ?>"
                    alt="<?php echo $image['alt']; ?>" title="<?php echo $image['title']; ?>" />
                  <?php endif; ?>

                </div>
                <div class="card-body ">
                  <header class="entry-header room-type text-center">
                    <h4 class=' display-3 phosphate-h1'>
                      <?php the_sub_field('title'); ?>
                    </h4>
                    <em><?php the_sub_field('subtitle'); ?></em>
                  </header>
                  <div class="pad25 text-left" style="padding-left: 20px;"><?php the_sub_field('list'); ?></div>
                  <div class="room-type card-body ">
                    <div class=" iconrow">
                      <?php
                    if (have_rows('iconrep')):
                      while (have_rows('iconrep')) : the_row();
                    ?>
                      <img class="image-responsive" style="max-height: 48px; padding: 5px;"
                        src="<?php the_sub_field('icon'); ?>" alt="<?php the_field('title'); ?>">
                      <?php
                      endwhile;
                    else :
                      // no rows found
                        ?>
                      <img class="image-responsive"
                        src="<?php echo get_template_directory_uri() . '/img/rooms/' . get_field('number_people') . '-icon.png'; ?>"
                        alt="<?php the_field('title'); ?>">
                      <?php
                    endif;
                    ?>
                    </div>
  
                  </div>
                </div>
                
              </div>
              <!--/panel-->
            </div>
            <!--/col-->

            <?php endwhile; ?>
          </div>
        </div>
      </div>
      <!--/row-->
    </div>
  </section>


  <section class="hstop whitebox skewmarginbottom">
    <div class="se-content container-fluid fullheightarea Alignera ">
      <div class="row" style="  align-self: start;">
        <div class="col-xs-12 col-md-10 col-md-offset-1 ">
          <div class="row" style="  align-self: start;">
            <div class="col-xs-12 col-md-6 pad80 ">
              <div class="bluecontact ">
                <h3><?php the_field('contact_us_title'); ?></h3>
                <p><?php the_field('contact_us_description'); ?></p>
                <?php
    if (get_field('contact_form')):
       echo do_shortcode(get_field('contact_form'));
    else:
        echo do_shortcode('[contact-form-7 id="1876" title="Site Group Contact Email Form - on group page"]');
    endif;
 ?>
                <div class="captext">
                  <small>This site is protected by reCAPTCHA and the Google
                    <a href="https://policies.google.com/privacy">Privacy Policy</a> and
                    <a href="https://policies.google.com/terms">Terms of Service</a> apply.</small>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-xs-12 text-right mobile-margin-top pad80">
              <p><?php the_field('our_group_department'); ?></p>

              <hr class="groupdivider center-block text-right">
              <h4><?php the_field('reservation_includes_title'); ?></h4>
            </div>
            <div class="col-md-6 col-xs-12 text-right  mobile-margin-bottom ">
              <div class="row sm-icon-row">
                <?php  while (has_sub_field('include_fields')): ?>

                <div class="col-xs-12 col-sm-6">
                  <div class="">
                    <div class="text-right sm-iconholder">
                      <em><?php the_sub_field('title'); ?></em>
                      <i class="fa <?php the_sub_field('icon'); ?>" aria-hidden="true"></i>
                    </div>

                  </div>
                </div>

                <?php endwhile; ?>
              </div>
            </div>
          </div>


        </div>

      </div>
    </div>
</div>
</div>
</section>

<!--

<p class="full-width-flex">Select<br />
    [select* your-subject "general information" "reservation request" "reservation change"] </p>
<p>
-->



<!--
	<div id="nya" class="nya">
		<p class="nya-text"><?php the_field('book_now_in_sub-offer_pages_pop_up', 'option'); ?></p>
	</div>
-->

<!-- #main -->

<?php
endwhile;

get_footer();
?>
<script>
  jQuery(document).ready(function ($) {
    $(".departure input, .arrival input").datepicker({
      dateFormat: "dd/mm/yy",
      autoSize: true,
      gotoCurrent: true,
      showAnim: "slideDown",
      beforeShow: function () {
        if (!$('.datepicker_wrapper').length) {
          $('#ui-datepicker-div').wrap('<div class="input-date-datepicker-control"></div>');
        }
      }
    });

  });
</script>