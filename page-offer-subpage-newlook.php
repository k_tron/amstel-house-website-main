<?php
/*
Template Name: Offer - Subpage-newlook
*/
get_header(); ?>
<style type="text/css">
.fill-height-or-more {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
  -webkit-flex-direction: column;
  -moz-box-orient: vertical;
  -moz-box-direction: normal;
  -ms-flex-direction: column;
  /* flex-direction: row; */
  -ms-flex-pack: distribute;
  justify-content: space-around;
  min-height: calc(100% - 60px) ;
}
.fill-height-or-more > div {
  -webkit-box-flex: 1;
  -ms-flex: 1;
  flex: 1;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: center;
  -ms-flex-pack: center;
  justify-content: center;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
  -ms-flex-direction: column;
  flex-direction: column;
  -webkit-box-pack: space-evenly;
  -ms-flex-pack: space-evenly;
  justify-content: space-evenly;
  /* height: auto; */
  margin-top: auto;
  margin-bottom: auto;
}
.text-content {
  margin: 20px 0px;
}
.fullheightarea2 {
  min-height: 100vh;
  height: 100%;
  background-size: contain;
  background-repeat: no-repeat;
}
.popup .book-room-now {
  margin-bottom: 30px;
  text-align: center;
}
.popup .offer-feature a.button {
  background-color: hsl(59, 91%, 51%);
  color: black;
  width: auto;
  display: inline-block;
  /* margin: 0 auto; */
  padding: 10px 30px;
}
.span4 img.alignnone {
  /* position: relative; */
  display: inline;
  /* float: right; */
  margin-bottom: -10px;
  margin-left: 10px;
}
.popup .text-content p {
  margin-bottom: 5%;
  text-align: left;
  margin-top: 5%;
}
.popup ul {
  text-align: left;
  /* list-style: circle; */
  margin-left: 15px;
  list-style-type: disc;
  margin-bottom: 30px;
}
.popup .text-content {
  margin: 0px 3vw;
}
.text-content h2 {
  margin: 1vw 0;
}
@media (max-width: 768px) {
  .fill-height-or-more > div {
    display: block;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    -ms-flex-preferred-size: 100%;
    flex-basis: 100%;
  }
  .fill-height-or-more {
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    display: block;
  }
  .text-content {
    text-align: left;
    margin-top: 50px;
  }
  .popup .text-content {
    padding-top: 50px;
  }
}
.col-xs-12.col-md-6.nopadding.sixtyflex {
  flex-grow: 1;
  flex-shrink: 1;
  flex: 1 1 33%;
}
</style>
<?php if (have_posts()) : while (have_posts()) : the_post();?>
  <div id="headerwrap">
    <div class="booknow">
      <img src="<?php echo get_template_directory_uri(); ?>/img/bookhere.gif" alt="book here" title="book">
    </div>
  </div>
    <div class="popup people funstuff" role="main">
      <section class="roomspage2 offer-feature">
        <div class="container-fluid">
          <div class="row some-area fill-height-or-more" style="  flex-direction: row-reverse;">
            <div class="col-xs-12 col-md-6">
              <div class="text-content fill-height-or-more">
                <div class="welcome-body span4 collapse-group" style="text-align: left;">
                  <div class="">
                    <h1><?php the_title(); ?></h1>
                    <div class="titlepop">
                      <h2 style="color: black;"><?php the_field('sub_title'); ?></h2>
                    </div>
                    <p class=" titlepop"><?php echo get_the_content();?></p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-md-6 nopadding sixtyflex">
              <div class=" fillwidth fullheightarea2" style="background-image: url(<?php the_field('left_image'); ?>);"></div>
            </div>
          </div>
        </div>
      </section>
    </div>
<?php endwhile; endif; ?>
<?php get_footer(); ?>
