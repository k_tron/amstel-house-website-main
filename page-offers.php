<?php
/*
Template Name: Offer Page
*/

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>
  <div id="headerwrap">
    <div class="booknow">
      <img src="<?php echo get_template_directory_uri(); ?>/img/bookhere.gif" alt="book here" title="book">
    </div>
    <?php
  $img_src = wp_get_attachment_image_url( get_post_thumbnail_id( $post->ID ), 'full' );
  $img_srcset = wp_get_attachment_image_srcset( get_post_thumbnail_id( $post->ID ), 'full' );
  $img_alt = get_post_meta( get_post_thumbnail_id( $post->ID ), '_wp_attachment_image_alt', true);
  ?>
    <img class=" mobilesmaller fillwidth fullheightarea" src="<?php echo esc_url( $img_src ); ?>"
         srcset="<?php echo esc_attr( $img_srcset ); ?>"
         sizes="(max-width: 1024px) 100vw, 1024w" alt="<?php echo $img_alt; ?>">
    <!-- <h1 class="header-title"><?php the_title(); ?></h1> -->
    <?php
  $link_f = get_field( 'main_image_link', 'option' );
  if ( $link_f ) {
  $link = $link_f;
  } else {
  $link = '/best-price-guarantee';
  }
  ?>
    <a href="<?php echo esc_url( $link ); ?>">
      <div class="hidden-xs">
        <p class="hostel-adrress">
          <?php the_field('line_below_slider', 'option'); ?>
        </p>
      </div>
      <div class="visible-xs-block">
        <p class="hostel-adrress">
          <?php the_field('mobile_line_below_slider', 'option'); ?>
        </p>
      </div>
    </a>
  </div>
  <div id="main" class="aboutus offerspage" role="main">
    <div class="container fullheightarea Aligner">
      <div class="row">
        <div class="col-xs-12 col-md-8 col-md-offset-2 my-2">
          <div class="welcome">
            <div class="welcome-body span4 collapse-group">
              <div class="intro">
                <?php the_content(); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <?php
    // Get Children Pages (Offer Subpages)
    // $args = array(
    //     'post_type'      => 'page',
    //     'posts_per_page' => 100,
    //     'no_found_rows'  => true,
    //     'post_parent'    => $post->ID,
    //     'order'          => 'ASC',
    //     'orderby'        => 'menu_order'
    //  );
    //
    //
    // $parentpage = new WP_Query( $args );
    $frontpage_id = get_option('page_on_front');
    //$val = get_post_meta( get_the_ID(), 'meta_data_name', true );
  //  echo $frontpage_id;
  ?>
  <div id="offers" class="se-container se-slope up iconsboxa offersbox blackbox">

      <div class="se-content container-fluid Aligner">
          <div class="row">
              <div class="col-xs-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
                  <div class="Grid offerspadder" style="  max-width: 100%;">
                      <?php
                      if( have_rows('homeoffers', $frontpage_id) ):
                      $a = 0;
                      $image_size = 'full';
                      while (have_rows('homeoffers', $frontpage_id) ) : the_row();
                      $image_id = get_sub_field('icon', $frontpage_id);
                      $image_array = wp_get_attachment_image_src($image_id, $image_size);
                      $image_url = $image_array[0];
                      ?>
                      <div class="halfdiv Grid-item three">
                              <a id="homeoffers-text-<?php echo $a; ?>" href="<?php the_sub_field('link', $frontpage_id); ?>"
                                  class="badgea left" style=
                                  "background-image: url('<?php echo $image_url; ?>');">
                                  <div  class="hiddenholderas hh-home">
                                      <span class="img-circle">
                                          <em><span class="firstc"><?php the_sub_field('line_1', $frontpage_id); ?></span>
                                          <span class="secondc"><?php the_sub_field('line_2', $frontpage_id); ?></span>
                                          </em>
                                      </span>
                                  </div>
                                  <div class="hiddentext">
                                    <?php $theid = url_to_postid(get_sub_field('link', $frontpage_id));

                                      echo "<h4>" . get_field('preview_title', $theid) . "</h4>";
                                      the_field('preview_detail', $theid); ?>
                                  </div>
                              </a>
                          </div><?php $a++; endwhile; endif;?>
                      </div>
                  </div>
              </div>
          </div>
      </div>


</div>
<!-- #main -->

<?php
endwhile;


get_footer();
