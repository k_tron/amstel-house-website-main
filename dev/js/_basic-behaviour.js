(function($) {


	
	 $(document).ready(function() {
		 $('#nav-toggle').on('click', function(){
			 $(this).toggleClass('active');
		 });
    });
    
 

    /* Offer group back button */
    if ($('.page-template-page-offer-subpage').find('.parent-link').length > 0) {
        $('.page-template-page-offer-subpage').find('.parent-link').on('click', function() {
            if ($('.assd-tool:visible').length > 0) {
                $('.assd-tool').hide();
                $('.single-offer').find('.main-text, img').show();
                return false
            }
        });
    }


    /* Revise to make the black box reservation page go straight to system */
/*
    $('.page-template').find('.book-now-box').find('form').on('submit', function() {
        // If Validation Pass
          console.log('here2');
        $('.book-now-box, .mask-2').hide();
    });
*/

})(jQuery);


/*!
 * Custom
 */
// Placeholder


!function(e){e.fn.shorten=function(s){"use strict";var t={showChars:100,minHideChars:10,ellipsesText:"...",moreText:"more",lessText:"less",onLess:function(){},onMore:function(){},errMsg:null,force:!1};return s&&e.extend(t,s),e(this).data("jquery.shorten")&&!t.force?!1:(e(this).data("jquery.shorten",!0),e(document).off("click",".morelink"),e(document).on({click:function(){var s=e(this);return s.hasClass("less")?(s.removeClass("less"),s.html(t.moreText),s.parent().prev().animate({height:"0%"},function(){s.parent().prev().prev().show()}).hide("fast",function(){t.onLess()})):(s.addClass("less"),s.html(t.lessText),s.parent().prev().animate({height:"100%"},function(){s.parent().prev().prev().hide()}).show("fast",function(){t.onMore()})),!1}},".morelink"),this.each(function(){var s=e(this),n=s.html(),r=s.text().length;if(r>t.showChars+t.minHideChars){var o=n.substr(0,t.showChars);if(o.indexOf("<")>=0){for(var a=!1,i="",h=0,l=[],c=null,f=0,u=0;u<=t.showChars;f++)if("<"!=n[f]||a||(a=!0,c=n.substring(f+1,n.indexOf(">",f)),"/"==c[0]?c!="/"+l[0]?t.errMsg="ERROR en HTML: the top of the stack should be the tag that closes":l.shift():"br"!=c.toLowerCase()&&l.unshift(c)),a&&">"==n[f]&&(a=!1),a)i+=n.charAt(f);else if(u++,h<=t.showChars)i+=n.charAt(f),h++;else if(l.length>0){for(j=0;j<l.length;j++)i+="</"+l[j]+">";break}o=e("<div/>").html(i+'<span class="ellip">'+t.ellipsesText+"</span>").html()}else o+=t.ellipsesText;var m='<div class="shortcontent">'+o+'</div><div class="allcontent">'+n+'</div><div class="moreholder"><a href="javascript://nop/" class="morelink">'+t.moreText+"</a></div>";s.html(m),s.find(".allcontent").hide(),e(".shortcontent p:last",s).css("margin-bottom",0)}}))}}(jQuery);

/**
 * jQuery Plugin to obtain touch gestures from iPhone, iPod Touch and iPad, should also work with Android mobile phones (not tested yet!)
 * Common usage: wipe images (left and right to show the previous or next image)
 * 
 * @author Andreas Waltl, netCU Internetagentur (http://www.netcu.de)
 * @version 1.1.1 (9th December 2010) - fix bug (older IE's had problems)
 * @version 1.1 (1st September 2010) - support wipe up and wipe down
 * @version 1.0 (15th July 2010)
 */
(function($){$.fn.touchwipe=function(settings){var config={min_move_x:20,min_move_y:20,wipeLeft:function(){},wipeRight:function(){},wipeUp:function(){},wipeDown:function(){},preventDefaultEvents:true};if(settings)$.extend(config,settings);this.each(function(){var startX;var startY;var isMoving=false;function cancelTouch(){this.removeEventListener('touchmove',onTouchMove);startX=null;isMoving=false}function onTouchMove(e){if(config.preventDefaultEvents){e.preventDefault()}if(isMoving){var x=e.touches[0].pageX;var y=e.touches[0].pageY;var dx=startX-x;var dy=startY-y;if(Math.abs(dx)>=config.min_move_x){cancelTouch();if(dx>0){config.wipeLeft()}else{config.wipeRight()}}else if(Math.abs(dy)>=config.min_move_y){cancelTouch();if(dy>0){config.wipeDown()}else{config.wipeUp()}}}}function onTouchStart(e){if(e.touches.length==1){startX=e.touches[0].pageX;startY=e.touches[0].pageY;isMoving=true;this.addEventListener('touchmove',onTouchMove,false)}}if('ontouchstart'in document.documentElement){this.addEventListener('touchstart',onTouchStart,false)}});return this}})(jQuery);


jQuery(document).ready(function ($) {

           var moretext = "<div class=''>More</div>";
    var lesstext = "<div class=''>Less</div>";
    
    $(".viewtoggle").click(function(){
        if($(".more", this).hasClass("less")) {
            $(".more", this).removeClass("less").addClass('more');
            $(".more", this).html(text_readmore);
        } else {
            $(".more", this).addClass("less");
            $(".more", this).html(text_readless);
        }
      //  $(this).parent().prev().toggle();
       // $(this).prev().toggle();
//          return false;
        });

         
        $(".fancybox").fancybox({
       padding: 0,
    margin: 0,
    autoSize: false,
    autoResize: true,
    nextEffect: 'elastic',
    prevEffect: 'elastic',
    fitToView: false,
    openEffect: 'fade',
    openMethod: "changeIn",
    openSpeed: 400,
    autoCenter: true,
     tpl: {
        closeBtn: '<i class="material-icons closebox">close</i>'
    },
    afterShow  : function () {
        jQuery('.fancybox-wrap').touchwipe({
             wipeLeft: function() { jQuery.fancybox.next(); },
             wipeRight: function() { jQuery.fancybox.prev() },        
             min_move_x: 20,
             min_move_y: 20,
             preventDefaultEvents: true
         });
           },
    mouseWheel: false
          });
          
/*
    $("a.fancyofferbox").each(function(){
        var hrefParts = $(this).attr('href').split('#');
        $(this).attr('href', hrefParts[0] + '?type=iframe#' + hrefParts[1]);
        });
*/
        
      $("a.fancyofferbox").fancybox({
    padding: 0,
    margin: 0,
    autoSize: false,
    autoResize: true,
    nextEffect: 'elastic',
    prevEffect: 'elastic',
    fitToView: false,
    openEffect: 'fade',
    openMethod: "changeIn",
    openSpeed: 400,
    autoCenter: true,
     tpl: {
        closeBtn: '<i class="material-icons closebox">close</i>'
    },
    mouseWheel: false,
       height: $(window).height(),
       beforeShow: function() {
             $.extend(this, {
        aspectRatio: false,
        //  type    : 'html',
        width: '100%',
        height: '100%',
        // content : '<div class="fancybox-image" style="background-image:url(' + this.href + '); background-size: cover; background-position:50% 50%;background-repeat:no-repeat;height:100%;width:100%;" /></div>'
      });

    },
    
    helpers: {
      media: {},
      title: {
        type: 'float' // 'float', 'inside', 'outside' or 'over'
      }
    }
    });

  $(window).width() > 640 && $("p.tel a").attr("href", ""),
  $(".page-template-page-offer-subpage").find(".parent-link").length > 0 && $(".page-template-page-offer-subpage").find(".parent-link").on("click", function() {
    if ($(".assd-tool:visible").length > 0)
      return $(".assd-tool").hide(),
      $(".single-offer").find(".main-text, img").show(),
      !1
  }),
  $(".page").find(".booking-left").find("form").on("submit", function() {
      $(".orderbox-box").hide().removeClass("orderbox-box-shown");
        console.log('turning off');
            console.log('closed');
     
       $('.booking-left').removeClass("showbox");
        $('.popholder').html($('.booking-left')).removeClass('popholder');
        $(".mask-2").remove();
         $.fancybox.close();
         
  });   
    
if( $('body').hasClass("page-template-page-room")){
    

$(".moretext").shorten({
    showChars: 140,
    moreText: "<div class='more2'>" + text_readmore + "</div>",
    lessText: "<div><div class='more2 less'>" + text_readless + "</div></div>"
    
    
});

}


});




