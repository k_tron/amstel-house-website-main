<section id="footer" class=" greyboxa section base">
	<div id="contact" class="mainskew greybox basea">
		<div class=" se-slope up iconsbox">
			<div class="se-content container  ">
				<div class="row">
					<div class="col-xs-12">
						<div class=" text-center center-block">
							<div class="h1">Stay<?php // the_field('footertextone', 'option'); ?></div>

							<div class="h2">IN CONTACT<?php // the_field('footer_text_two', 'option'); ?></div>
							<div itemscope itemtype="http://schema.org/Hostel">
								<span class="hidden" itemprop="name">Amstel House Hostel Berlin</span>
								<span class="hidden" itemprop="url"><?php echo site_url(); ?></span>
								<img class="hidden" itemprop="logo" src="https://amstelhouse.de/img/logo.png" alt="hotel logo" />
								<img class="hidden" itemprop="image" src="https://amstelhouse.de/img/logo.png" alt="hotel logo" />
								<a itemprop="hasMap" class="hidden" href="https://www.google.de/maps/place/Amstel+House+Hostel+Berlin/@52.5283768,13.3345458,17z/data=!3m1!4b1!4m5!3m4!1s0x47a8510d2652e53b:0xe67055cb7aa023ec!8m2!3d52.5283768!4d13.3367345" target="_blank"></a>

								<p>

									<span class="hidden-xs hidden-sm" itemprop="telephone"><?php the_field('contact_phone_number', 'option'); ?></span>
									<span class="hidden-md hidden-lg" ><a href="tel:<?php the_field('contact_phone_number', 'option'); ?>"><?php the_field('contact_phone_number', 'option'); ?></a></span>
									<br />
									<span itemprop="email"><a data-form="contact-email-form" href="mailto:<?php the_field('contact_email', 'option'); ?>" ><?php the_field('contact_email', 'option'); ?></a></span>
									<br />
									<span class="hostel-name" style="display: none;" itemprop="name"><?php the_field('contact_venue_name', 'option'); ?></span>
									<a href="https://www.google.de/maps/place/Amstel+House+Hostel+Berlin/@52.5283768,13.3345458,17z/data=!3m1!4b1!4m5!3m4!1s0x47a8510d2652e53b:0xe67055cb7aa023ec!8m2!3d52.5283768!4d13.3367345" target="_blank">
										<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
										<span itemprop="streetAddress"><?php the_field('contact_address1', 'option'); ?></span><br />
										<span itemprop="postalCode"><?php the_field('contact_address2', 'option'); ?></span>
										<span itemprop="addressLocality"><?php the_field('contact_address3', 'option'); ?></span>
										<span class="hidden" itemprop="addressCountry">Germany</span>
										</div>
									</a>

								</p>
								<div itemprop="geo" itemscope itemtype="http://schema.org/GeoCoordinates">
	 <meta itemprop="latitude" content="52.5283768" />
	 <meta itemprop="longitude" content="13.3345458" />
 </div>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-md-10 col-md-offset-1">
						<div class="row header-social-links">
							<?php while (has_sub_field('header_links', 'option')): ?>
							<div class="col-xs-2 col-md-1">
								<div class="text-center pad25">
									<a href="<?php the_sub_field('link_url'); ?>" target="_blank" class="<?php the_sub_field('link_color'); ?>" rel="noopener">
										<?php if (get_sub_field('link_name') == "globe" || get_sub_field('link_name') == "Blog" ) { ?>
												<i class="fa fa-lg fa-globe-europe"></i>
									<?php	} else { ?>
										<i class="fab fa-lg fa-<?php echo strtolower(get_sub_field('link_name')); ?>"></i>
										<?php } ?>
									</a>
								</div>
							</div>
							<?php endwhile; ?>
						</div>
					</div>




					</div> <!-- row -->
				</div>


		<div class="meta-nav hidden-xsa">
			<div id="navbar2" class="navbar-collapse collapse">
				<?php
				$args2 = array(
				//                 'theme_location' => 'header-menu',
				'menu' => 'footer',
				//                     'depth'      => 2,
				'container'  => false,
				'menu_class'     => 'nav navbar-nav navbar-center',
				'walker'     => new Bootstrap_Walker_Nav_Menu()
				);
				if (has_nav_menu('header-menu'))
				{
				wp_nav_menu($args2);
				}
				?>
			</div>
			<!--
			<?php if
			(get_field('footer_links', 'option')): ?>
			<ul class="links">
				<?php while
				(has_sub_field('footer_links', 'option')): ?>
				<li><a target="<?php the_sub_field('link_target'); ?>" href="<?php the_sub_field('link_url'); ?>" target="_blank"><?php the_sub_field('link_name'); ?></a></li>
				<?php endwhile; ?>
			</ul>
			<?php endif; ?>
			-->
		</div>
		<div class="copyrighta">
			<span class="copy-icon">©</span> Amstel House
		</div>
			</div>
		</div>

	</div>
</section>
</div>

<?php wp_footer(); ?>
<div class="orderbox-box" style="display: none;"></div>
