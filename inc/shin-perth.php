<?php

// load acf pages.
if( function_exists( 'acf_add_options_page' ) ) {

	acf_add_options_page( 'AH Global Options' );

	acf_add_options_page( 'AH Common Words' );

}


function custom_msls_output_get( $url, $link, $current ) {
	$result = json_encode(
		array(
			'language' 	=> $link->alt,
			'url'		=> $url,
		)
	);
    return $result;
}
add_filter( 'msls_output_get', 'custom_msls_output_get', 10, 3 );
