<?php
/*
Template Name:  Hygiene
*/
get_header();
?>
<style type="text/css">
.fill-height-or-more {
display: -webkit-box;
display: -ms-flexbox;
display: flex;
-webkit-box-orient: vertical;
-webkit-box-direction: normal;
-ms-flex-direction: column;
flex-direction: row;
-ms-flex-pack: distribute;
justify-content: space-around;
min-height: calc(100% - 60px);
-ms-flex-wrap: wrap;
flex-wrap: wrap;
}
.fill-height-or-more > div {
display: -webkit-box;
display: -ms-flexbox;
display: flex;
-webkit-box-pack: center;
-ms-flex-pack: center;
justify-content: center;
-webkit-box-orient: vertical;
-webkit-box-direction: normal;
-ms-flex-direction: column;
flex-direction: column;
-webkit-box-pack: space-evenly;
-ms-flex-pack: space-evenly;
justify-content: space-evenly;
}
.text-content {
margin: 20px 0px;
}
.fullheightarea2 {
min-height: 100vh;
height: 100%;
}
.popup .book-room-now {
margin-bottom: 30px;
text-align: center;
}
.popup .offer-feature a.button {
background-color: hsl(59, 91%, 51%);
color: black;
width: auto;
display: inline-block;
/* margin: 0 auto; */
padding: 10px 30px;
}
.span4 img.alignnone {
/* position: relative; */
display: inline;
/* float: right; */
margin-bottom: -10px;
margin-left: 10px;
}
.popup .text-content p {
margin-bottom: 5%;
text-align: left;
margin-top: 5%;
}
.popup ul {
text-align: left;
/* list-style: circle; */
margin-left: 15px;
list-style-type: disc;
margin-bottom: 30px;
}
.popup .text-content {
margin: 0px 5vw;
}
.text-content h1 {
margin: 1vw 0;
}
.mobilesmaller.fillwidth.aboutimage {
min-height: 70vh;
background-size: auto 100%;
background-position: center;
-ms-flex-item-align: start;
align-self: flex-start;
justify-self: flex-start;
vertical-align: top;
margin-bottom: auto;
}
.aboutpage .header-title {
font-size: 3vw;
/* margin-left: 10px; */
bottom: 0px;
margin: auto;
text-align: left;
padding-left: 20px;
/* word-wrap: break-word; */
width: 100%;
line-height: 1em;
margin-bottom: 8px;
/* display: flex; */
/* margin-top: auto; */
/* bottom: 0px; */
/* height: 100%; */
/* flex: 1 1 100%; */
position: absolute;
color: white;
}
.room-type_pop {
font-size: 1.1em;
height: 100%;
}
.titlepop h1, .titlepop h2 {
font-size: 5.9em;
font-weight: bold;
/* font-family: "proxima_nova_bold"; */
font-family: 'Phosphate-s',arial,sans-serif;
letter-spacing: 2px;
color: black;
}
.titlepop h2 {
color: black;
font-size: 1.5em;
font-weight: 100;
font-family: proxima_nova_bold-con;
}
.titlepop {
padding: 3vw 0px;
}
.aboutpage p {
margin: 0 15px;
color: black;
font-size: 1em;
}
.shortcontent {
/*  height: 2em;*/
}
.teamtext {
padding: 25px 0px;
letter-spacing: 0.01em;
margin-top: 0px;
-ms-flex-item-align: start;
align-self: flex-start;
margin-bottom: auto;
}
.teamtext .moretext {
height: 100%;
/* flex: 1; */
-webkit-box-align: normal;
-ms-flex-align: normal;
align-items: normal;
/* justify-items: center; */
display: -webkit-box;
display: -ms-flexbox;
display: flex;
-webkit-box-flex: 1;
-ms-flex: 1;
flex: 1;
-webkit-box-orient: vertical;
-webkit-box-direction: normal;
-ms-flex-direction: column;
flex-direction: column;
}
.viewtoggle {
font-size: .7em;
/* font-weight: bold; */
}
/*
.roomspage {
padding-right: 20px;
padding-left: 20px;
}
*/
.moreholder {
text-align: center;
}
a.morelink {
display: block;
text-align: center;
}
.popup.people.aboutpage {
padding-left: 3vw;
padding-right: 3vw;
}
.mobilesmaller > img {
object-fit: contain;
max-height: 400px; /* 75vh; */
margin: 0 auto;
/* align-self: center; */
/* justify-content: center; */
/* justify-items: center; */
display: block;
}
.rsArrow .rsArrowIcn {
/* filter: invert(100%); */
background-color: initial !important;
}
.teamtext p {
font-size: 1em;
}
@media (max-width: 768px) {
.aboutpage {
overflow-x: hidden;
}
.popup.people.aboutpage {
padding-left:15px;
padding-right: 15px;
}
.fill-height-or-more > div {
display: block;
-ms-flex-wrap: wrap;
flex-wrap: wrap;
-ms-flex-preferred-size: 100%;
flex-basis: 100%;
}
.fill-height-or-more {
-ms-flex-wrap: wrap;
flex-wrap: wrap;
display: block;
}
.text-content {
text-align: left;
margin-top: 50px;
}
.popup .text-content {
padding-top: 50px;
margin: 0px auto;
}
.titlepop {
padding: 3em 0px;
font-size: .9em;
text-align: center;
}
}
@media (max-width: 992px) {
.aboutpage .header-title {
font-size: 3.2em
}
}
/*  Timeline  */
.timeline {
list-style: none;
padding: 60px 0 20px;
position: relative;
}
@media (min-width: 768px) {
.timeline:before {
top: 0;
bottom: 0;
position: absolute;
content: " ";
width: 3px;
background-color: #eeeeee;
left: 50%;
margin-left: -1.5px;
}
.timeline > li {
margin-bottom: 20px;
position: relative;
}
.timeline > li:before,
.timeline > li:after {
content: " ";
display: table;
}
.timeline > li:after {
clear: both;
}
.timeline > li:before,
.timeline > li:after {
content: " ";
display: table;
}
.timeline > li:after {
clear: both;
}
.timeline > li > .timeline-panel {
width: 50%;
float: left;
border: 1px solid #d4d4d4;
border-radius: 2px;
padding: 20px;
position: relative;
-webkit-box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
box-shadow: 0 4px 0px rgba(0, 0, 0, 0.175);
}
.timeline > li.timeline-inverted + li:not(.timeline-inverted),
.timeline > li:not(.timeline-inverted) + li.timeline-inverted {
margin-top: -60px;
}
.timeline > li:not(.timeline-inverted) {
padding-right: 120px;
}
.timeline > li.timeline-inverted {
padding-left:120px;
}
.timeline > li > .timeline-panel:before {
position: absolute;
top: 26px;
right: -15px;
display: inline-block;
border-top: 15px solid transparent;
border-left: 15px solid #ccc;
border-right: 0 solid #ccc;
border-bottom: 15px solid transparent;
content: " ";
}
.timeline > li > .timeline-panel:after {
position: absolute;
top: 27px;
right: -14px;
display: inline-block;
border-top: 14px solid transparent;
border-left: 14px solid #fff;
border-right: 0 solid #fff;
border-bottom: 14px solid transparent;
content: " ";
}
.timeline > li > .timeline-badge {
color: #fff;
width: 80px;
height: 80px;
line-height: 80px;
font-size: 1.4em;
text-align: center;
position: absolute;
top: 5px;
left: 50%;
margin-left: -40px;
background-color: transparent;
z-index: 100;
border-top-right-radius: 50%;
border-top-left-radius: 50%;
border-bottom-right-radius: 50%;
border-bottom-left-radius: 50%;
padding: 0px;
/* border: 3px solid #eee; */
}
.timeline > li.timeline-inverted > .timeline-panel {
float: right;
}
.timeline > li.timeline-inverted > .timeline-panel:before {
border-left-width: 0;
border-right-width: 15px;
left: -15px;
right: auto;
}
.timeline > li.timeline-inverted > .timeline-panel:after {
border-left-width: 0;
border-right-width: 14px;
left: -14px;
right: auto;
}
}
.timeline-badge.primary {
background-color: #2e6da4 !important;
}
.timeline-badge.success {
background-color: #3f903f !important;
}
.timeline-badge.warning {
background-color: #f0ad4e !important;
}
.timeline-badge.danger {
background-color: #d9534f !important;
}
.timeline-badge.info {
background-color: #5bc0de !important;
}
.timeline-title {
margin-top: 0;
color: inherit;
}
.timeline-body > p,
.timeline-body > ul {
margin-bottom: 0;
}
.timeline-body > p + p {
margin-top: 5px;
}
.timeline-badge {
text-align: center;
padding: 10px;
background: #f2e964;
}
/* video */
.embed-container {
position: relative;
padding-bottom: 56.25%;
overflow: hidden;
max-width: 100%;
height: auto;
}
.embed-container iframe,
.embed-container object,
.embed-container embed {
position: absolute;
top: 0;
left: 0;
width: 100%;
height: 100%;
}
.tworows {
display: flex;
flex-direction: column;
white-space: pre-wrap;
}
</style>
<?php
$i = 0;
if (have_posts()) : while (have_posts()) : the_post();
?>
<div id="headerwrap">
	<div class="booknow">
		<img src="<?php echo get_template_directory_uri(); ?>/img/bookhere.gif" alt="book here" title="book">
	</div>
</div>
<div class="popup people aboutpage ">
	<div class="container-fluid  titlepop">
		<div class="row">
			<div class="col-xs-12 text-center ">
				<h2><?php the_title();?></h2>
			</div>
		</div>
	</div>
</div>
<div id="main" class="aboutus" role="main">
	<div class="container fullheightarea0 Aligner">
		<div class="row">
			<div class="col-xs-12 col-md-8 col-md-offset-2">
				<div class="tworows">
					<div class="welcome-body span4 collapse-group">
						<div class="intro">
							<?php the_content(); ?>
							
						</div>
					</div>
					<div class="embed-container">
						<?php the_field('video'); ?>
					</div>
					
					
				</div>
			</div>
		</div>
	</div>
	<section id="steps">
		<div class="container-fluid se-container roomspage">
			<div class="row">
				<div class="col-xs-12 col-md-10 col-md-offset-1">
					<ul class="timeline">
						<?php while(has_sub_field('steps')):
						$images = get_sub_field('icon');
						$class = ( $i % 2 == 0 ) ? 'timeline-inverted' : '';
						// vars
										//	$img_src = wp_get_attachment_image_url( the_sub_field('image'), 'full' );
						?>
						
						<li class="aboutrowa <?=$class;?>">
							<div class="timeline-badge">
								<?php
								//  $images = get_field('images');
								if( $images ):
								$img_srcset = wp_get_attachment_image_srcset($images[0]['ID']);
								$url = $images['url'];
								$size = 'full';
								$thumb = $images[0]['sizes'][ $size ];
								?>
								<img class="aboutimg_0" src="<?php echo $url; ?>" alt="<?php echo $images['alt']; ?>" srcset="<?php echo $img_srcset; ?>" data-alt="<?php echo $image['title'];?>" />
								<?php
								endif; ?>
							</div>
							<div class="timeline-panel">
								<!-- 	<div class="timeline-heading">
										<h4 class="timeline-title">title</h4>
								</div> -->
								<div class="timeline-body">
									<p>
										<?php the_sub_field('description'); ?>
									</p>
								</div>
							</div>
						</li>
						<?php
						$i++;
						endwhile; ?>
					</ul>
					<div class="intro">
						<a href="<?php the_field('pdf'); ?>">
							<h2>Download PDF</h2>
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	
</div>
<?php endwhile; endif; ?>
<?php get_footer(); ?>