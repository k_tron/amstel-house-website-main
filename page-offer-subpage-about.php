<?php
/*
Template Name: Offer - Subpage-about
*/
get_header();
?>
<style type="text/css">
  .fill-height-or-more {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: row;
    -ms-flex-pack: distribute;
    justify-content: space-around;
    min-height: calc(100% - 60px);
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
  }
  .fill-height-or-more > div {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    justify-content: center;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
    -webkit-box-pack: space-evenly;
    -ms-flex-pack: space-evenly;
    justify-content: space-evenly;
  }
  .text-content {
    margin: 20px 0px;
  }
  .fullheightarea2 {
    min-height: 100vh;
    height: 100%;
  }
  .popup .book-room-now {
    margin-bottom: 30px;
    text-align: center;
  }
  .popup .offer-feature a.button {
    background-color: hsl(59, 91%, 51%);
    color: black;
    width: auto;
    display: inline-block;
    /* margin: 0 auto; */
    padding: 10px 30px;
  }
  .span4 img.alignnone {
    /* position: relative; */
    display: inline;
    /* float: right; */
    margin-bottom: -10px;
    margin-left: 10px;
  }
  .popup .text-content p {
    margin-bottom: 5%;
    text-align: left;
    margin-top: 5%;
  }
  .popup ul {
    text-align: left;
    /* list-style: circle; */
    margin-left: 15px;
    list-style-type: disc;
    margin-bottom: 30px;
  }
  .popup .text-content {
    margin: 0px 5vw;
  }
  .text-content h1 {
    margin: 1vw 0;
  }
  .mobilesmaller.fillwidth.aboutimage {
    min-height: 70vh;
    background-size: auto 100%;
    background-position: center;
    -ms-flex-item-align: start;
    align-self: flex-start;
    justify-self: flex-start;
    vertical-align: top;
    margin-bottom: auto;
  }
  .aboutpage .header-title {
    font-size: 3vw;
    /* margin-left: 10px; */
    bottom: 0px;
    margin: auto;
    text-align: left;
    padding-left: 20px;
    /* word-wrap: break-word; */
    width: 100%;
    line-height: 1em;
    margin-bottom: 8px;
    /* display: flex; */
    /* margin-top: auto; */
    /* bottom: 0px; */
    /* height: 100%; */
    /* flex: 1 1 100%; */
    position: absolute;
    color: white;
  }
  .room-type_pop {
    font-size: 1.1em;
    height: 100%;
  }
  .titlepop h1, .titlepop h2 {
    font-size: 5.9em;
    font-weight: bold;
    /* font-family: "proxima_nova_bold"; */
    font-family: 'Phosphate-s',arial,sans-serif;
    letter-spacing: 2px;
    color: black;
  }
  .titlepop h2 {
    color: black;
    font-size: 1.5em;
    font-weight: 100;
    font-family: proxima_nova_bold-con;
  }
  .titlepop {
    padding: 3vw 0px;
  }
  .aboutpage p {
    margin: 0 15px;
    color: black;
    font-size: 1em;
  }
  .shortcontent {
    /*  height: 2em;*/
  }
  .teamtext {
    padding: 25px 0px;
    letter-spacing: 0.01em;
    margin-top: 0px;
    -ms-flex-item-align: start;
    align-self: flex-start;
    margin-bottom: auto;
  }
  .teamtext .moretext {
    height: 100%;
    /* flex: 1; */
    -webkit-box-align: normal;
    -ms-flex-align: normal;
    align-items: normal;
    /* justify-items: center; */
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-flex: 1;
    -ms-flex: 1;
    flex: 1;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
  }
  .viewtoggle {
    font-size: .7em;
    /* font-weight: bold; */
  }
  /*
  .roomspage {
  padding-right: 20px;
  padding-left: 20px;
  }
  */
  .moreholder {
    text-align: center;
  }
  a.morelink {
    display: block;
    text-align: center;
  }
  .popup.people.aboutpage {
    padding-left: 3vw;
    padding-right: 3vw;
  }

.mobilesmaller > img {
  object-fit: contain;
  max-height: 400px; /* 75vh; */
  margin: 0 auto;
  /* align-self: center; */
  /* justify-content: center; */
  /* justify-items: center; */
  display: block;
}



.rsArrow .rsArrowIcn {
  /* filter: invert(100%); */
  background-color: initial !important;
}

.teamtext p {
  font-size: 1em;
}

  @media (max-width: 768px) {
    .aboutpage {
      overflow-x: hidden;
    }
    .popup.people.aboutpage {
      padding-left:15px;
      padding-right: 15px;
    }
    .fill-height-or-more > div {
      display: block;
      -ms-flex-wrap: wrap;
      flex-wrap: wrap;
      -ms-flex-preferred-size: 100%;
      flex-basis: 100%;
    }
    .fill-height-or-more {
      -ms-flex-wrap: wrap;
      flex-wrap: wrap;
      display: block;
    }
    .text-content {
      text-align: left;
      margin-top: 50px;
    }
    .popup .text-content {
      padding-top: 50px;
      margin: 0px auto;
    }
    .titlepop {
      padding: 3em 0px;
      font-size: .9em;
      text-align: center;
    }
  }
  @media (max-width: 992px) {
    .aboutpage .header-title {
      font-size: 3.2em
    }
  }
</style>
<?php
register_new_royalslider_files(1);
if (have_posts()) : while (have_posts()) : the_post();
?>
<div id="headerwrap">
  <div class="booknow">
    <img src="<?php echo get_template_directory_uri(); ?>/img/bookhere.gif" alt="book here" title="book">
  </div>
  <?php
$img_src = wp_get_attachment_image_url( get_post_thumbnail_id( $post->ID ), 'full' );
$img_srcset = wp_get_attachment_image_srcset( get_post_thumbnail_id( $post->ID ), 'full' );
$img_alt = get_post_meta( get_post_thumbnail_id( $post->ID ), '_wp_attachment_image_alt', true);
?>
  <img class=" mobilesmaller fillwidth fullheightarea" src="<?php echo esc_url( $img_src ); ?>"
       srcset="<?php echo esc_attr( $img_srcset ); ?>"
       sizes="(max-width: 1024px) 100vw, 1024w" alt="<?php echo $img_alt; ?>">
  <!-- <h1 class="header-title"><?php the_title(); ?></h1> -->
  <?php
$link_f = get_field( 'main_image_link', 'option' );
if ( $link_f ) {
$link = $link_f;
} else {
$link = '/best-price-guarantee';
}
?>
  <a href="<?php echo esc_url( $link ); ?>">
    <div class="hidden-xs">
      <p class="hostel-adrress">
        <?php the_field('line_below_slider', 'option'); ?>
      </p>
    </div>
    <div class="visible-xs-block">
      <p class="hostel-adrress">
        <?php the_field('mobile_line_below_slider', 'option'); ?>
      </p>
    </div>
  </a>
</div>
<div id="main" class="aboutus" role="main">
  <div class="container fullheightarea Aligner">
    <div class="row">
      <div class="col-xs-12 col-md-8 col-md-offset-2">
        <div class="welcome">
          <div class="welcome-body span4 collapse-group">
            <div class="intro">
              <?php the_content(); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid roomspage some-area Aligner " >
    <div class="row fill-height-or-more">
      <?php while(has_sub_field('articles')):
$images = get_sub_field('image');
// vars
//	$img_src = wp_get_attachment_image_url( the_sub_field('image'), 'full' );
?>
<div class="col-xs-12  aboutrow  bg-holder">
      <div class="container my-1">
        <div class="row">
          <div class="col-lg-6 col-md-12 p-x2">
            <div class="mobilesmaller">
              <?php
//  $images = get_field('images');
if( $images ):
if (is_array($images)) {
// at lease 1 image

if (count($images) == 1) {
// only 1 image

              $img_srcset = wp_get_attachment_image_srcset($images[0]['ID']);
              $url = $images['url'];
              $size = 'medium';
              $thumb = $images[0]['sizes'][ $size ];
              ?>
              <img class="aboutimg_0" src="<?php echo $thumb; ?>" alt="<?php echo $images['alt']; ?>" srcset="<?php echo $img_srcset; ?>" data-alt="<?php echo $image['title'];?>" />
              <?php
} else {
// more than 1 image

?>
              <div class="new-gallery badgec left">
                <div class="royalSlider new-royalslider-6 rsDefault rs-default-template2 rsHor" style="  width: 100%;
                                                                                                       ">
                  <!-- simple image -->
                  <?php foreach( $images as $image ):
$img_srcset = wp_get_attachment_image_srcset($image['ID']);
?>
                  <!-- <img class="rsImg" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" srcset="<?php echo $img_srcset; ?>" data-alt="<?php echo $image['title'];?>" /> -->
                  <a class="rsImg" href="<?php echo $image['url']; ?>">
                    <?php echo $image['alt']; ?>
                  </a>
                  <?php endforeach; ?>
                </div>
              </div>
              <?php
}
}
endif; ?>
            </div>
          </div>
          <div class="col-lg-6 col-md-12 p-x2">
            <h3 class="header-title">
              <?php the_sub_field('title'); ?>
            </h3>
            <div class="room-type_pop">
              <div class="teamtext">
                <?php the_sub_field('description'); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
      </div>
      <?php endwhile; ?>
    </div>
  </div>

</div>
<?php endwhile; endif; ?>
<?php get_footer(); ?>
<script type="text/javascript">
  $(document).ready(function() {
    $(".royalSlider").royalSlider({
      // autoHeight: false,
      autoScaleSlider: true,
    autoScaleSliderWidth: 548,
    autoScaleSliderHeight: 400,
    autoPlay: {
   enabled: true,
   pauseOnHover: true,
delay: 4000
},

    /* size of all images http://help.dimsemenov.com/kb/royalslider-jquery-plugin-faq/adding-width-and-height-properties-to-images */
    // imgWidth: 792,
    // imgHeight: 479

      lazyLoading:!0,
      imageWidth:'100%',imageHeight:'100%',
      controlNavigation: 'bullets',
      imageScaleMode: 'fill',
      imageAlignCenter: true,
      loop: false,
      loopRewind: false,
      numImagesToPreload: 0,
      keyboardNavEnabled: true,
      autoScaleSlider: true,
      imageScalePadding:0,arrowsNavHideOnTouch:!0,loop:!0,numImagesToPreload:0,sliderDrag:1,navigateByClick:1,fadeinLoadedSlide:!1
    }
                                 );
  }
                   );
</script>
