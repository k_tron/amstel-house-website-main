<?php
/**
* The header for our theme.
header("Access-Control-Allow-Origin: *");
*
* Displays all of the <head> section and everything up till <div id="content">
*
* @package Amstelhouse
*/


/* Get Prefered language of the browser */
if (!isset($Langue)) {
	$Langue = explode(',',$_SERVER['HTTP_ACCEPT_LANGUAGE']);
	$Langue = strtolower(substr(chop($Langue[0]),0,2));
}
/* Check if the visitor requests the root of you multisite blog (ie "/")
 * In this case, the visitor is redirected to the language specific home depending of the language 
 * Change /fr/ and /en/ by the name of your subsites */
if ($_SERVER['REQUEST_URI'] == "/") {

	if ($Langue == "fr") {
		header("Location: /fr/");
	}
	elseif ($Langue == "de") {
	//	header("Location: /");
	}
	elseif ($Langue == "en") {
		header("Location: /en/");
	}
	elseif ($Langue == "es") {
		header("Location: /es/");
	}
	elseif ($Langue == "it") {
		header("Location: /it/");
	}	
	elseif ($Langue == "nl") {
		header("Location: /nl/");
	}
	elseif ($Langue == "pl") {
		header("Location: /pl/");
	}
	elseif ($Langue == "pt") {
		header("Location: /pt/");
	}
	elseif ($Langue == "ru") {
		header("Location: /ru/");
	}
	else {
		header("Location: /en/");
	}
}
/* End of the Hack */


?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="preload" href="<?php echo get_stylesheet_directory_uri(); ?>/fonts/proxima_nova-bold-webfont.woff2" as="font" crossorigin="anonymous">
	<link rel="preload" href="<?php echo get_stylesheet_directory_uri(); ?>/fonts/Phosphate-Inline.woff2" as="font" crossorigin="anonymous">
	<link rel="preload" href="<?php echo get_stylesheet_directory_uri(); ?>/fonts/Phosphate-Solid.woff2" as="font" crossorigin="anonymous">
	<!-- <link rel="preload" href="<?php echo get_stylesheet_directory_uri(); ?>/fonts/fontawesome-webfont.woff2" as="font" crossorigin="anonymous"> -->
	<meta name="google-site-verification" content="d1HAhCH_bGGSgMgx7Mves7SHe7EuZSDGc3WH2bdSflk" />
	<meta name="audience" content="all" />
	<meta name="p:domain_verify" content="07c6a136bcb6389d26c1e02620584f1d"/>
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon.png" />
	<!-- Google Tag Manager -->
	<script>
	(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-K8ZN3P4');
</script>
	<!-- End Google Tag Manager -->
	<?php wp_head();
	global $lang_of_site;
	 global	$language; 
	$lang_of_site = explode('/', $_SERVER['REQUEST_URI'])[1];
	if ($lang_of_site == "") {
		$lang_of_site = "de";
	}
	$blog     = MslsBlogCollection::instance()->get_current_blog();
	if($blog){
	 $language = $blog->get_language();
	}
	?>
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('set', 'anonymizeIp', true);
		ga('create', 'UA-4866257-1', 'auto');
		ga('send', 'pageview');
		</script>
		<!-- Facebook Pixel Code -->
		<script>
			!function(f,b,e,v,n,t,s)
			{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
				n.callMethod.apply(n,arguments):n.queue.push(arguments)};
				if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
				n.queue=[];t=b.createElement(e);t.async=!0;
				t.src=v;s=b.getElementsByTagName(e)[0];
				s.parentNode.insertBefore(t,s)}(window,document,'script',
				'https://connect.facebook.net/en_US/fbevents.js');
				fbq('init', '140607729918919');
				fbq('track', 'PageView');
				</script>
				<noscript>
					<img height="1" width="1"
					src="https://www.facebook.com/tr?id=140607729918919&ev=PageView
					&noscript=1"/>
				</noscript>
				<!-- End Facebook Pixel Code -->
			</head>
			<body <?php body_class(); ?>>
				<!-- Google Tag Manager (noscript) -->
				<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K8ZN3P4"
					height="0" width="0" style="display:none;visibility:hidden"></iframe>
				</noscript>
					<!-- End Google Tag Manager (noscript) -->
					<header>
						<div class="meta-nav">
						</div><!-- .meta-nav -->
						<nav role="navigation">
							<div class="navbar navbar-default "> <!-- navbar-fixed-top  -->
								<div class="container-fluid-no">
									<!-- .navbar-toggle is used as the toggle for collapsed navbar content -->
									<div class="navbar-header">
										<div id="logo">
											<a href="<?php echo home_url(); ?>/" title="<?php bloginfo( 'name' ); ?>" rel="home">
												<img src="<?php echo get_template_directory_uri(); ?>/img/logo.svg" alt="<?php bloginfo( 'name' ) ?>" />
											</a>
										</div>
									</div>
									<div class="mobilelang dropdown meta-nav lang-switcher dropdown-menu-right">
										<div class=" dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											<a class="mainflag <?php echo $language; ?>"></a>
										</div>
										<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
											<ul>
												<?php
												$swap_wp_to_older = array(
													'de_DE' => array(
														'code'  => 'de',
														'human' => 'Deutsch',
														'order' => 5
													),
													'us' => array(
														'code'  => 'en',
														'human' => 'English',
														'order' => 10
													),
													'es_ES' => array(
														'code'  => 'es',
														'human' => 'Español',
														'order' => 15
													),
													'fr_FR' => array(
														'code'  => 'fr',
														'human' => 'Français‎',
														'order' => 20
													),
													'it_IT' => array(
														'code'  => 'it',
														'human' => 'Italiano',
														'order' => 25
													),
													'nl_NL' => array(
														'code'  => 'nl',
														'human' => 'Nederlands',
														'order' => 30
													),
													'pl_PL' => array(
														'code'  => 'pl',
														'human' => 'Język polski',
														'order' => 35
													),
													'pt_PT' => array(
														'code'  => 'pt',
														'human' => 'Português',
														'order' => 40
													),
													'ru_RU' => array(
														'code'  => 'ru',
														'human' => 'русский',
														'order' => 45
													),
												);
												$languagesOptions = array();
												$mslsObject = new MslsOutput();
												foreach ($mslsObject->get(0) as $option) {
													$languagesOptions[] = json_decode($option, true);
												}
												/*
												echo '<pre> here';
												var_dump( $languagesOptions );
												echo '</pre>';
												*/
												foreach ( $swap_wp_to_older as $key => $value ) {
													$prepare = array();
													foreach ( $languagesOptions as $msls ) {
														if ( $key == $msls['language'] ) {
															$prepare['class'] = $value['code'];
															$prepare['url']   = $msls['url'];
															$prepare['human'] = $value['human'];
															?>
															<li class="<?php echo esc_attr( $prepare['class'] ); ?>"><a class=" dropdown-item" href="<?php echo esc_url( $prepare['url'] ); ?>"><?php echo esc_attr( $prepare['human'] ); ?></a></li>
															<?php
														}
													}
												}
												?>
											</ul>
										</div>
									</div>
									<button type="button" id="nav-toggle" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse"><span></span>
									</button>
									<div class="navbar-collapse collapse navbar-center navbar-responsive-collapse">
										<div class="flex-nav">
											<?php
											$args = array(
												'theme_location' => 'header-menu',
												//                     'depth'      => 2,
												'container'  => false,
												'menu_class'     => 'nav navbar-nav ',
												'walker'     => new Bootstrap_Walker_Nav_Menu()
											);
											if (has_nav_menu('header-menu')) {
												wp_nav_menu($args);
											}
											?>
											<div class="desktoplang dropdown meta-nav lang-switcher dropdown-menu-right">
												<div class=" dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													<a class="mainflag <?php echo $language; ?>"></a>
												</div>
												<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
													<ul>
														<?php
														$swap_wp_to_older = array(
															'de_DE' => array(
																'code'  => 'de',
																'human' => 'Deutsch',
																'order' => 5
															),
															'us' => array(
																'code'  => 'en',
																'human' => 'English',
																'order' => 10
															),
															'es_ES' => array(
																'code'  => 'es',
																'human' => 'Español',
																'order' => 15
															),
															'fr_FR' => array(
																'code'  => 'fr',
																'human' => 'Français‎',
																'order' => 20
															),
															'it_IT' => array(
																'code'  => 'it',
																'human' => 'Italiano',
																'order' => 25
															),
															'nl_NL' => array(
																'code'  => 'nl',
																'human' => 'Nederlands',
																'order' => 30
															),
															'pl_PL' => array(
																'code'  => 'pl',
																'human' => 'Język polski',
																'order' => 35
															),
															'pt_PT' => array(
																'code'  => 'pt',
																'human' => 'Português',
																'order' => 40
															),
															'ru_RU' => array(
																'code'  => 'ru',
																'human' => 'русский',
																'order' => 45
															),
														);
														$languagesOptions = array();
														$mslsObject = new MslsOutput();
														foreach ($mslsObject->get(0) as $option) {
															$languagesOptions[] = json_decode($option, true);
														}
														/*
														echo '<pre> here';
														var_dump( $languagesOptions );
														echo '</pre>';
														*/
														foreach ( $swap_wp_to_older as $key => $value ) {
															$prepare = array();
															foreach ( $languagesOptions as $msls ) {
																if ( $key == $msls['language'] ) {
																	$prepare['class'] = $value['code'];
																	$prepare['url']   = $msls['url'];
																	$prepare['human'] = $value['human'];
																	?>
																	<li class="<?php echo esc_attr( $prepare['class'] ); ?>"><a class=" dropdown-item" href="<?php echo esc_url( $prepare['url'] ); ?>"><?php echo esc_attr( $prepare['human'] ); ?></a></li>
																	<?php
																}
															}
														}
														?>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</nav>
						<?php
						?>
					</header><!-- #branding -->
					<?php if( is_front_page() ) {
						$hidebookingw = "showbooking";
					} else {
						$hidebookingw = "hidebooking";
					}
					?>
					<div id="booking-desktopframe" class="popbox <?=$hidebookingw;?>">
						<section id="booking-widget-left" class="booking-widget booking-left">
							<a class="phoneicon" href="tel:+49 030 395 40 72"><div><i class="fa fa-phone" aria-hidden="true"></i></div></a>
							<a class="phoneicon" style="left: 50px;" href="https://www.google.de/maps/place/Amstel+House+Hostel+Berlin/@52.5283768,13.3345458,17z/data=!3m1!4b1!4m5!3m4!1s0x47a8510d2652e53b:0xe67055cb7aa023ec!8m2!3d52.5283768!4d13.3367345"><div><i class="fa fa-map" aria-hidden="true"></i></div></a>
							<div id="mobile-toggle" class="booking-widget__form__submit">
								<div class="booking-widget__title"><?php the_field('text_book_now_here', 'option'); ?></div>
								<div class="close-x" alt="close booking">
									<i class="material-icons">close</i>
								</div>
								<!--
								<span class="ubtn-data ubtn-icon hidden" style="position: absolute; top: 15px; right: 20px;">
								<i class="Defaults-close" style="font-size:18px;color: #fff9e7;"></i>
								</span>  -->
							</div>
							<div class="mobile-hide">
								<form class="booking-widget__form" method="get" action="https://hostels.assd.com/tabs.php" data-action-mobile="https://hostels.assd.com/mobile.php" target="_blank">
									<div class="booking-widget__form__row-hidden">
										<input type="hidden" name="cust" value="921131" />
										<input type="hidden" name="type" value="020" />
										<input type="hidden" name="house" value="410230">
										<?php
										$langcrs = "E";
										if ( 'de_DE' == get_locale() ) {
											$langcrs = "D";
										}
										?>
										<input type="hidden" name="langcrs" value="<?php echo $langcrs; ?>">
										<input type="hidden" name="checkauto" value="1">
									</div>
									<div class="booking-widget__form__row">
										<input id="arrival_date" class="booking-widget__form__date booking-widget__form__date--arrival" type="date" name="check-in" value="" placeholder="<?php the_field('text_arrival', 'option'); ?>" required=""> </div>
										<div class="booking-widget__form__row">
											<input id="departure_date" class="booking-widget__form__date booking-widget__form__date--departure" type="date" name="check-out" value="" placeholder="<?php the_field('text_departure', 'option'); ?>" required=""> </div>
											<div class="booking-widget__form__row">
												<select class="booking-widget__form__guests" data-placeholder="<?php the_field('text_persons', 'option'); ?>" name="guests" required="">
													<option></option>
													<option value="1">1</option>
													<option value="2">2</option>
													<option value="3">3</option>
													<option value="4">4</option>
													<option value="5">5</option>
													<option value="6">6</option>
													<option value="7">7</option>
													<option value="8">8</option>
													<option value="9">9</option>
													<option value="10">10</option>
													<option value="11">11</option>
													<option value="12">12+</option>
												</select>
											</div>
											<div class="booking-widget__form__row">
												<select class="booking-widget__form__child" data-placeholder="<?php the_field('text_kids', 'option'); ?>" name="child1">
													<option value=""></option>
													<option value="1">1</option>
													<option value="2">2</option>
													<option value="3">3</option>
													<option value="4">4</option>
													<option value="5">5</option>
													<option value="6">6</option>
													<option value="7">7</option>
													<option value="8">8</option>
													<option value="9">9</option>
													<option value="10">10</option>
													<option value="11">11</option>
													<option value="12">12 +</option>
												</select>
											</div>
											<div class="booking-widget__form__submit" translate="no">
												<button type="submit" name="button"><?php the_field('text_book_now', 'option'); ?></button>
											</div>
										</form>
									</div>
								</section>
							</div>
							<div id="booking-mobileframe" class="popbox"></div>
							<div class="bookingifcontainer">
								<div class="close-x">
									<i class="material-icons">close</i>
								</div>
								<div id="booking-iframe">
								</div>
							</div>
							<div id="wrapper" class="clearfix <?php echo "lang_". $lang_of_site; ?>">
