<?php
  /*
  Template Name: Front Page
  */
  get_header(); ?>
<?php while (have_posts()):
  the_post(); ?>
<div id="headerwrap">
  <div class="booknow">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/bookhere.gif?v=5" alt="book here" title="book" width="120" srcset="https://www.amstelhouse.de/wp-content/themes/amstelhouse-staging/img/bookhere_x2.gif 2x">
  </div>
  <?php
    if (wp_is_mobile()) {
        /* Display and echo mobile specific stuff here */
        register_new_royalslider_files(get_field('slider_id_mobile'));
        echo get_new_royalslider(get_field('slider_id_mobile'));
    }
    else {
        register_new_royalslider_files(2);
        echo get_new_royalslider(2);
    }
    $link_f = get_field('main_image_link', 'option');
    if ($link_f) {
        $link   = $link_f;
    }
    else {
        $link   = '/best-price-guarantee';
    }
    ?>
  <script>
    var text_readmore = "<?php the_field('text_more', 'option'); ?>";
    var text_readless = "<?php the_field('text_readless', 'option'); ?>";
    if(text_readless == "") {
    text_readless = "Read Less";
    }
  </script>
  <a href="<?php echo esc_url($link); ?>">
    <div class="hidden-xs">
      <p class="hostel-adrress"><?php the_field('line_below_slider', 'option'); ?></p>
    </div>
    <div class="visible-xs-block">
      <p class="hostel-adrress"><?php the_field('mobile_line_below_slider', 'option'); ?></p>
    </div>
  </a>
</div>
<div id="main" class=" homepage " role="main">
  <div class="container-fluid fullheightarea Aligner" style="z-index: -1">
    <div class="row">
      <div class="col-xs-12 col-sm-10 col-sm-offset-1">
        <div class="welcome">
          <div class="welcome-body span4">
            <div><?php the_field('welcome_text_above'); ?>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <section id="specialoffers" class="z1 z1-75 mainskew align-items-center" style="background-image: url('<?php echo get_template_directory_uri();?>/img/door-orange-listras.jpg">
    <div class="absheaderskew center-block  yellowboxa blackonwhite-text">
      <h3 class="was-h2" >
        <?php if (!get_field('special_offers_header')) {
          echo "Story";
          }
          else {
          the_field('special_offers_header');
          } ?>
      </h3>
    </div>
    <div class="z2 container" style="background-image: url('<?php echo get_template_directory_uri();?>/img/door-orange-listras.jpg">
      <div class="row auto-clear">
        <div class="col-xs-12 auto-clear">
          <div class="panel panel-default">
            <div class="text-content hexagon_container">
              <a href="<?php if(get_field('offerslink')){ the_field('offerslink'); }else { echo get_permalink(12); } ?>">
              <div class="hexagon">
                <div class="hextestx">
              <h2 class="photsphate-in was-h3 ">   <?php the_field('special_offers_header_copy'); ?></h2>
              <?php the_field('special_offers'); ?>
              </div>
            </div>
          </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section id='rooms-section' class="z1 z1-75 mainskew flexcol" style="">
    <div class="absheaderskew center-block  yellowboxa whiteonblack-text hidden-xs">
      <h3 class="was-h2" >
        <?php if (!get_field('rooms_and_beds_header')) {
          echo "Story";
          }
          else {
          the_field('rooms_and_beds_header');
          } ?>
      </h3>
    </div>
    <img class="absimg" src="
      <?php
        $image = get_field('rooms_image');
        echo $image['url'];
        ?>">
    <div class="z3 my-1">

      <div class="container-fluid">
        <div class="row auto-clear">
          <div class="col-sm-8 col-xs-12 auto-clear">
            <div class="panel panel-default">
              <div class="text-content">
                <!-- <h3 class="was-h2" >ROOMS & BEDS </h2> -->
             <a href="<?php echo ((get_field('roomsandbedslink')) ? the_field('roomsandbedslink') : get_permalink(46)); ?>"> 
							 <h2 class="photsphate-in was-h3 ">  <?php the_field('rooms_and_beds_header_copy'); ?>  </h2>
						 </a>
                <?php the_field('rooms_beds'); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <?php if (get_field('stories_header')) { ?>
  <section id="story" class="z1 z1-75 mainskew yellowboxa basea story">
    <div class="absheaderskew center-block yellowboxa blackonwhite-text">
      <h3 class="was-h2" >
        <?php if (!get_field('stories_header')) {
          //  echo "Story";
          the_field('story_head');
          }
          else {
          the_field('stories_header');
          } ?>
      </h3>
    </div>
    <div class="se-container se-slope up iconsbox offersbo">
      <div class="storyholder invskew ">
        <div class="abscenter_story">
            <h2 class="photsphate-in was-h3  text-white">  
							 <a href="<?php echo ((get_field('aboutlink')) ? the_field('aboutlink') : get_permalink(1986)); ?>"> <?php the_field('stories_sub_heading'); ?> </a> 
						 </h2>

        </div>
        <?php // check if the repeater field has rows of data
          if (have_rows('story_row_1')):
              // loop through the rows of data
              $tworow = 0;
              while (have_rows('story_row_1')):
                  the_row();

                  if ($tworow % 2 == 0) {
          ?>
        <div class="col card smallbox  justify-items-center mx-0">
          <?php
            }
            ?>
          <div class="storypart">
            <hr>
            <h3 class="entry-title card-title">
              <?php the_sub_field('header'); ?>
            </h3>
            <p><?php the_sub_field('content'); ?></p>
            <?php
              $image = get_sub_field('photo');
              if ($image):
              ?>
            <?php echo wp_get_attachment_image($image, 'thumbnail', "", ["class" => "my-custom-class"]); ?>
            <?php
              endif ?>
          </div>
          <?php
            $tworow++;
            if ($tworow % 2 == 0) {
            ?>
        </div>
        <?php
          }
          endwhile;
          else:
          // no rows found

          endif;
          ?>
      </div>
    </div>
  </section>
  <?php
    } ?>
  <section class="mainskew yellowbox base pullup services_home ">
    <div class="se-container se-slope up iconsbox offersbo">
      <div class="absheaderskew center-block  yellowboxa blackonpink-text">
        <h3 class="was-h2" ><?php the_field('section_title_service'); ?></h3>
      </div>
      <div class="se-content container-fluid fullheightarea Aligner">
        <div class="row">
          <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 group">
            <div class="Grid">
              <?php while (has_sub_field('top_page_gallery_images')): ?>
              <div class="halfdiv Grid-item three group">
                <div class="spanner">
                  <a href="<?php the_field('link_to_the_gallery_page'); ?>">
                    <div class="img-circle">
                      <?php
                        $image = get_sub_field('image');
                        if ($image):
                        ?>
                      <img src="<?php echo $image['sizes']['thumbnail']; ?>" title="<?php echo $image['title'] ?>" alt="<?php echo $image['alt'] ?>">
                      <?php
                        endif ?>
                    </div>
                    <div class="room-typea">
                      <div class="text-content">
                        <h6><?php the_sub_field('title'); ?></h6>
                      </div>
                    </div>
                  </a>
                </div>
              </div>
              <?php
                endwhile; ?>

                  <div class="text-content container ">

                    <h2 class="photsphate-in was-h3 "> <?php the_field('services_text_copy'); ?></h2>
                    <p style="  max-width: 733px;  margin: 0 auto;"><?php the_field('services_text'); ?></p>
                  </div>

            </div>


            <!-- <a style="display: block; text-align: center;" href="<?php the_field('link_to_the_gallery_page'); ?>">
              <div class="viewtoggle more" style="width: auto;   display: inline-block;">
                <?php the_field('section_learnmore_service'); ?>
              </div>
            </a> -->
          </div>
        </div>
      </div>
    </div>
  </section>
  <section id="blog" class="mainskew yellowbox2 base">
    <?php $posts = my_prefix_fetch_data('posts?per_page=3'); ?>
    <div class="se-container se-slope up iconsbox offersbo">
      <div class="right12margin center-block textskew yellowbox2">
        <h3 class="was-h2" >BLOG</h3>
      </div>
      <div class="se-content container-fluid fullheightarea Aligner">
        <div class="col-xs-12 col-sm-12  col-md-12 center-block col-lg-10">
          <div class="text-center center-block">
            <a class="blogbutton" href="<?php echo  ($language == "de_DE" ? 'https://www.amstelhouse.de/blog/de/' : 'https://www.amstelhouse.de/blog/'); ?> ">
              <h2>
             
                <?php if(get_field('blog_sub_title')) { the_field('blog_sub_title'); } else {  echo "What’s up in Berlin this week?"; }  //  What’s up in Berlin this week?
                  ?>
              </h2>
            </a>
          </div>
          <div class="row evenheight">
            <?php
              foreach ($posts as $p) {
              ?>
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 mb-30">
              <div class="two-deg smallblog card border-primary h-100 bg-light ">
                <a  href="<?php echo $p->link; ?>">
                  <div class="square d-flex align-items-center justifty-content-center">
                    <?php echo $p->imagepost; ?>
                  </div>
                  <div class="card-body ">
                    <header class="entry-header">
                      <h5 class=' display-3 phosphate-h1'>
                        <?php echo $p
                          ->title->rendered; ?>
                      </h5>
                    </header>
                  </div>
                </a>
              </div>
            </div>
            <?php
              } ?>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section id="funstuff" class="mainskew hasimageina hasimagein imagefixedbg" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/geometrisch-berlin-fernsehen-turm-1_8color.png');">
    <div class="se-container se-slope up iconsbox  imagebox-y">
      <div class="right12margin center-block textskew yellowbox">
        <h3 class="was-h2" ><?php the_field('section_title'); ?></h3>
      </div>
      <div class="se-content container-fluid fullheightarea Aligner">
        <div class="row">
          <div class="col-xs-12 col-sm-10 col-sm-push-1 col-md-11 center-block col-lg-10 col-lg-push-1">
            <div class="row">

              <div class="col-md-12 col-lg-10 col-xs-12 pull-right">
                <div class="row equal">
                  <?php $b = 0;
                    while (has_sub_field('repeater_box')): ?>
                  <div class="col-sm-4 col-xs-12">
                    <div class="two-deg smallbox ">
                      <?php if (strpos(get_sub_field('link'), 'blog') == false) { ?>
                      <a id="homefun-text-<?php echo $b; ?>" href="<?php the_sub_field('link'); ?>"  class=" left fancyofferbox">
                      <?php
                        }
                        else { ?>
                      <a target="_blank" href="<?php the_sub_field('link'); ?>">
                        <?php
                          } ?>
                        <h3><?php the_sub_field('title'); ?></h3>
                        <hr>
                        <p><?php the_sub_field('desciption'); ?></p>
                      </a>
                    </div>
                  </div>
                  <?php $b++;
                    endwhile; ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<?php
  endwhile; ?>
<?php get_footer();
