<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package Amstelhouse
 */
?>

<div id="main" <?php post_class('main'); ?> role="main">

	<div class="page-header">
		<hgroup>
			<?php the_title( '<h1>', '</h1>' ); ?>
		</hgroup>
	</div>

	
	<div class="main-text">
		<?php the_content(); ?>
	</div>
	
</div><!-- #main -->
