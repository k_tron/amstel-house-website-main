<?php
/*
Template Name: Location Page
*/
get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
	<div id="headerwrap">
		<div class="booknow">
			<img src="<?php echo get_template_directory_uri(); ?>/img/bookhere.gif" alt="book here" title="book">
		</div>
		<?php
		$img_src = wp_get_attachment_image_url( get_post_thumbnail_id( $post->ID ), 'full' );
		$img_srcset = wp_get_attachment_image_srcset( get_post_thumbnail_id( $post->ID ), 'full' );
		$img_alt = get_post_meta( get_post_thumbnail_id( $post->ID ), '_wp_attachment_image_alt', true);
		?>
		<img class=" mobilesmaller fillwidth fullheightarea" src="<?php echo esc_url( $img_src ); ?>"
		srcset="<?php echo esc_attr( $img_srcset ); ?>"
		sizes="(max-width: 1024px) 100vw, 1024w" alt="<?php echo $img_alt; ?>">
		<div class="header-title"><?php the_title(); ?></div>
		<?php
		$link_f = get_field( 'main_image_link', 'option' );
		if ( $link_f ) {
			$link = $link_f;
		} else {
			$link = '/best-price-guarantee';
		}
		?>
		<a href="<?php echo esc_url( $link ); ?>">
			<div class="hidden-xs">
				<p class="hostel-adrress"><?php the_field('line_below_slider', 'option'); ?></p>
			</div>
			<div class="visible-xs-block">
				<p class="hostel-adrress"><?php the_field('mobile_line_below_slider', 'option'); ?></p>
			</div>
		</a>
	</div>
	<div id="main" class="location" role="main">
		<div class="container fullheightarea Aligner hidden-xs">
			<div class="row">
				<div class="col-xs-12 col-md-10 col-md-offset-1">
					<div class="welcome">
						<div class="welcome-body span4 collapse-group">
							<div class="intro">
								<?php the_content(); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<section class="mainskew hasimagein" style=" background-image: url(<?php the_field('map_location', 'option'); ?>); " >
			<a   target="_blank" href="https://www.google.de/maps/place/Amstel+House+Hostel+Berlin/@52.5283768,13.3345458,17z/data=!3m1!4b1!4m5!3m4!1s0x47a8510d2652e53b:0xe67055cb7aa023ec!8m2!3d52.5283768!4d13.3367345">
				<div class="se-container hasimageskew fullheightarea Aligner " >
					<div class="se-slope up imagebox-y abstitle">
						<div class="right12margin center-block textskew hidden-xs">
							<h2><?php the_field('maptitle', 'option'); ?></h2>
						</div>
						<div class="se-content container">
						</div>
					</div>
				</div>
			</a>
		</section>
		<section id="arrivals" class="mainskew yellowbox base">
			<?php if (get_field( 'arrival_list')): ?>
				<div class="se-container se-slope up iconsbox yellowbox yellowtext ">
					<div class="right12margin center-block textskew">
						<h2><?php the_field('page_head'); ?></h2>
					</div>
					<div class="se-content container fullheightarea Aligner">
						<div class="row">
							<div class="col-xs-12 col-md-10 col-md-offset-1">
								<h3 class="text-uppercase sectiontitle"><?php the_field('page_subhead'); ?> </h3>
								<div class="panel-group" id="accordion">
									<div class="panel  ">
										<div class="row">
											<?php $i=0 ; while (has_sub_field( 'arrival_list')): ?>
												<div class="panel-heading col-xs-6 col-md-3">
													<a href="#target<?php echo $i; ?>" class="spanner" data-toggle="collapse" data-parent="#accordion">
														<div class="img-circle"> <img src="<?php the_sub_field('arrival_image'); ?>"> </div>
														<h4><?php the_sub_field('arrival_title'); ?></h4> </a>
													</div>
													<?php $i++; endwhile; ?> </div>
													<?php $i=0 ; while (has_sub_field( 'arrival_list')): ?>
														<div id="target<?php echo $i; ?>" class="arrival-info panel-collapse collapse">
															<div class="panel-body">
																<?php the_sub_field( 'arrival_detail'); ?> </div>
															</div>
															<?php $i++; endwhile; ?> </div>
														</div>
														<!-- panel group -->
													</div>
												</div>
												<!-- container -->
											</div>
										</div>
									<?php endif; ?>
								</section>
								<?php if(get_field('loc_page_head')) { ?>
								<section id="location" class="mainskew blackbox base">
									<div class="se-container se-slope up iconsbox  yellowtext ">
										<div class="right12margin center-block textskew">
											<h2><?php the_field('loc_page_head'); ?></h2>
										</div>
										<div class="se-content container fullheightarea Aligner">
											<div class="row">
												<div class="col-xs-12 col-md-10 col-md-offset-1">
													<div class="row evenheight ">
														<?php while (has_sub_field('location_list')): ?>

															<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 ">
																<a href="<?php the_sub_field('link') ?>">
																	<div class="two-deg smallblog card border-primary h-100a bg-light roomspage2">
																		<div class="square d-flex align-items-center justifty-content-center">
																		<img class=image-responsive" <?php awesome_acf_responsive_image( get_sub_field('loc_image'),'medium','640px'); ?>" alt="<?php the_sub_field('loc_title'); ?>">
																			</div>

																		<div class="card-body ">
																			<header class="entry-header text-centera">
																				<h5 class=' display-3 phosphate-h1'>
																					<?php the_sub_field('loc_title'); ?>
																				</h3>
																				<p><?php the_sub_field('loc_subtitle'); ?></p>

																			</header>

																		</div>
																	</div>
																	</a>		
																</div>
															<?php endwhile; ?>
														</div>
													</div>
												</div>
											</div>
										</div>
									</section>
									<?php } ?>


									<!-- #main -->
									<?php
								endwhile;
								get_footer();
