<?php
/*
Template Name: Contact Page
*/
get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>


<div id="headerwrap">
<div class="booknow">
				<img src="<?php echo get_template_directory_uri(); ?>/img/bookhere.gif" alt="book here" title="book">
			</div>
 <?php
register_new_royalslider_files(2); 
echo get_new_royalslider(2); ?>
<h1 class="header-title"><?php the_title(); ?></h1>
			<p class="hostel-adrress"><?php the_field('line_below_slider', 'option'); ?></p>
	</div>

	<div id="main" class="location" role="main">
		<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-10 col-md-offset-1">
				<div class="welcome">
					<div class="welcome-body span4 collapse-group">
						<div class="intro">
								<h2><?php the_field( 'contact_description_bold' ); ?></h2>

							<p><?php the_field('contact_description'); ?></p>
							
    					</div>						

					</div>
				</div>
			</div>
		</div>
	</div>
	
	

<!-- #main -->
	</div>

<?php
endwhile;


get_footer();
