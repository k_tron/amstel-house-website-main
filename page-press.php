<?php
/*
Template Name: Press
*/
get_header();
 ?>
<style type="text/css">
.fill-height-or-more {
display: -webkit-box;
display: -ms-flexbox;
display: flex;
-webkit-box-orient: vertical;
-webkit-box-direction: normal;
-ms-flex-direction: column;
flex-direction: row;
-ms-flex-pack: distribute;
    justify-content: space-around;
min-height: calc(100% - 60px);
-ms-flex-wrap: wrap;
    flex-wrap: wrap;
}
.fill-height-or-more > div {

display: -webkit-box;
display: -ms-flexbox;
display: flex;
-webkit-box-pack: center;
-ms-flex-pack: center;
justify-content: center;
-webkit-box-orient: vertical;
-webkit-box-direction: normal;
-ms-flex-direction: column;
flex-direction: column;
-webkit-box-pack: space-evenly;
-ms-flex-pack: space-evenly;
justify-content: space-evenly;
/* height: auto; */
/* margin-top: auto; */
/* margin-bottom: auto; */
/* margin: 0px 0px; */
padding: 5px;
 /* align-items: flex-start;
align-self: flex-start; */
}
.text-content {
 margin: 20px 0px;
}
.fullheightarea2 {
min-height: 100vh;
height: 100%;
}
.popup .book-room-now {
margin-bottom: 30px;
text-align: center;
}
.popup .offer-feature a.button {
background-color: hsl(59, 91%, 51%);
color: black;
width: auto;
display: inline-block;
/* margin: 0 auto; */
padding: 10px 30px;
}
.span4 img.alignnone {
/* position: relative; */
display: inline;
/* float: right; */
  margin-bottom: -10px;
margin-left: 10px;
}
.popup .text-content p {
margin-bottom: 5%;
text-align: left;
  margin-top: 5%;
}
.popup ul {
text-align: left;
/* list-style: circle; */
margin-left: 15px;
list-style-type: disc;
margin-bottom: 30px;
}
.popup .text-content {
margin: 0px 5vw;
}
.text-content h2 {
margin: 1vw 0;
}
.mobilesmaller.fillwidth.aboutimage {
min-height: 50vh;
background-size: auto 100%;
background-position: center;
-ms-flex-item-align: start;
    align-self: flex-start;
justify-self: flex-start;
vertical-align: top;
margin-bottom: auto;
}
.aboutpage .header-title {
font-size: 3vw;
/* margin-left: 10px; */
bottom: 0px;
margin: auto;
text-align: left;
padding-left: 10px;
/* word-wrap: break-word; */
width: 100%;
line-height: 1em;
margin-bottom: 8px;

}
.room-type_pop {
font-size: 1.3em;
height: 100%;
}
.titlepop h2, .titlepop h3 {
font-size: 5.9em;
font-weight: bold;
/* font-family: "proxima_nova_bold"; */
font-family: 'Phosphate-s',arial,sans-serif;
letter-spacing: 2px;
color: black;
}
.titlepop h3 {
color: black;
font-size: 1.5em;
font-weight: 100;
font-family: proxima_nova_bold-con;
}
.titlepop {
padding: 3vw 0px;
}
.aboutpage p {
  margin: 0 15px;
color: black;
}
.shortcontent {
/*  height: 2em;*/
}
.teamtext {
padding: 25px 5px;
letter-spacing: 0.01em;
margin-top: 0px;
  -ms-flex-item-align: start;
      align-self: flex-start;
margin-bottom: auto;

}
.teamtext .moretext {
height: 100%;
/* flex: 1; */
-webkit-box-align: normal;
    -ms-flex-align: normal;
        align-items: normal;
/* justify-items: center; */
display: -webkit-box;
display: -ms-flexbox;
display: flex;
-webkit-box-flex: 1;
    -ms-flex: 1;
        flex: 1;
-webkit-box-orient: vertical;
-webkit-box-direction: normal;
    -ms-flex-direction: column;
        flex-direction: column;
}
.viewtoggle {
font-size: .7em;
/* font-weight: bold; */
}
/*
.roomspage {
padding-right: 20px;
padding-left: 20px;
}
*/
.moreholder {
text-align: center;
}
a.morelink {
display: block;
  text-align: center;
}
.popup.people.aboutpage {
padding-left: 3vw;
padding-right: 3vw;
}
@media (max-width: 768px) {
.aboutpage {
    overflow-x: hidden;
}
.popup.people.aboutpage {
   padding-left:15px;
padding-right: 15px;
}
.fill-height-or-more > div {
  display: block;
-ms-flex-wrap: wrap;
    flex-wrap: wrap;
-ms-flex-preferred-size: 100%;
    flex-basis: 100%;
}
.fill-height-or-more {
-ms-flex-wrap: wrap;
    flex-wrap: wrap;
      display: block;
}
.text-content {
text-align: left;
    margin-top: 50px;
}
.popup .text-content {
  padding-top: 50px;
    margin: 0px auto;
}
.titlepop {
padding: 3em 0px;
font-size: .9em;
text-align: center;
}
}
@media (max-width: 992px) {
.aboutpage .header-title {
  font-size: 3.2em
}
}
</style>
<?php if (have_posts()) : while (have_posts()) : the_post();?>
  <div id="headerwrap">
    <div class="booknow">
      <img src="<?php echo get_template_directory_uri(); ?>/img/bookhere.gif" alt="book here" title="book">
    </div>
  </div>
<div class="popup people aboutpage " style="height: 100%">
<div class="container-fluid  titlepop">
                    <div class="row">
	                    <div class="col-xs-12 text-center ">
		                    <h2><?php the_title();?></h2>
	                    </div>
                    </div><!--  // SHAPELESS BECOMING -->
</div>
<div class="container fullheightarea Aligner">
  <div class="row">
    <div class="col-xs-12 col-md-10 col-md-offset-1">
      <div class="welcome">
        <div class="welcome-body span4 collapse-group">
          <div class="intro">
            <?php the_content(); ?>
            <a href="<?php the_field('pdf'); ?>">
              <h2>Download PDF</h2>
            </a>

            <?php
            $images = get_field('press_images');

if( $images ): ?>
  <h2>Download Images</h2>

    <div class="row">
        <?php foreach( $images as $image ): ?>
            <div class="col-xs-12 col-md-4">
                <a href="<?php echo $image['url']; ?>">
                     <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
                </a>
                <p><?php echo $image['caption']; ?></p>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<?php endwhile; endif; ?>
<?php get_footer(); ?>
