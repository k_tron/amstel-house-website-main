<?php
/*
Template Name: FAQ

 *
 * @package Amstelhouse
 */

get_header(); ?>
<style>
.answer h3:before {

display: block; 
content: " "; 
margin-top: -65px;
height: 65px; 

}


 h3, h2 {
  line-height: inherit;
  font-family: proxima_nova_bold,arial,sans-serif;
}
.answer{
	font-family: proxima_nova,arial,sans-serif;
  font-size: 1.3em;
  line-height: 1.6em;
}


.question h2, .answer h2 {
  font-size: 30px !important;
  text-transform: none;

  font-weight: bold;
  color: #FF0066;
  line-height: 1.6em !important;
}

.welcome-body.span4.collapse-group {
  line-height: 1.8em;
}

.answer h3, .question h3 {
  color: black !important;
  font-weight: bold;
  font-size: 22px; 
  line-height: 1.5em;
  margin-top: 10px;
  letter-spacing: 0.02em
}

h3 a {
  /* text-transform: uppercase; */
  color: black !important;
}

 .question a h3 {
	color: #FF0066;
}

.answer h3 {
  color: #FF0066;
	margin-top: 15px;
	margin-bottom: 10px;
}
/* color: #5f5f5f !important; */
.popup .text-content p {
  /* margin-bottom: 5%; */
  text-align: left;
    /* margin-top: 5%; */
}
.popup ul {
  text-align: left;
  /* list-style: circle; */
  margin-left: 15px;
  list-style-type: disc;
  margin-bottom: 30px;
}
.popup .text-content {
  margin: 0px 5vw;
}
.text-content h2 {
  margin: 1vw 0;
}
.mobilesmaller.fillwidth.aboutimage {
  background-size: auto 100%;
  background-position: center;
  -ms-flex-item-align: start;
      align-self: flex-start;
  justify-self: flex-start;
  vertical-align: top;
  padding-bottom: 100%;
}
.mobilesmaller.fillwidth.aboutimage::before {
    content: "";
    float: left;
    padding-top: 100%;
}
.aboutpage .header-title {
font-size: 2.4vw;
  /* margin-left: 10px; */
  /* bottom: 0px; */
  margin: auto;
  text-align: left;
  /* padding-left: 10px; */
  /* word-wrap: break-word; */
  width: 100%;
  line-height: 1em;
  margin-bottom: 8px;
  padding: 15px 0px;
  text-align: center;
}
.room-type_pop {
  font-size: 1.3em;
}
.titlepop h1, .titlepop h2 {
  font-size: 5.9em;
  font-weight: bold;
  /* font-family: "proxima_nova_bold"; */
  font-family: 'Phosphate-s',arial,sans-serif;
  letter-spacing: 2px;
  color: black;
  text-align: center
}
.titlepop h2 {
  color: black;
  font-size: 1.5em;
  font-weight: 100;
  font-family: proxima_nova_bold-con;
}
.titlepop {
  padding: 3vw 0px;
}
.aboutpage p {
  font-size: 1.6em;
  text-transform: unset;
  color: black;
}
.teamtext {
  padding-top: 15px;
  margin-top: 0px;
}
.viewtoggle {
  font-size: .7em;
  /* font-weight: bold; */
}
/*
.roomspage {
  padding-right: 20px;
  padding-left: 20px;
}
*/
.moreholder {
  text-align: center;
}
a.morelink {
  display: block;
}
.popup.people.aboutpage {
  padding-left: 3vw;
  padding-right: 3vw;
}
.bottomsection h2 {
    font-size: 9em;
  font-family: 'Phosphate-in',arial,sans-serif;
  line-height: 1em;
  text-align: center;

}

span {
  color: #FF0066;
}

</style>
<?php if (have_posts()) : while (have_posts()) : the_post();?>
  <div id="headerwrap">
    <div class="booknow">
      <img src="<?php echo get_template_directory_uri(); ?>/img/bookhere.gif" alt="book here" title="book">
    </div>
  </div>
    <div class="popup people funstuff" role="main">
      <section class="roomspage2 offer-feature">
        <div class="container">
          <div class="row some-area fill-height-or-more" style="  flex-direction: row-reverse;">
            <div class="col-xs-12 col-md-12">
              <div class="text-content fill-height-or-more">
                <div class="welcome-body span4 collapse-group" style="text-align: left;">
                  
                    <h1><?php the_title(); ?></h1>
                  
                    <?php echo get_the_content();?>
                  
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-md-6 nopadding sixtyflex">
              <div class=" fillwidth fullheightarea2" style="background-image: url(<?php the_field('left_image'); ?>);"></div>
            </div>
          </div>
        </div>
      </section>
    </div>
<?php endwhile; endif; ?>
<?php get_footer(); ?>
