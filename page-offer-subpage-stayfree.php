<?php
/*
Template Name: Offer - Subpage-stayfree
*/
get_header();
?>
<!-- <script>
	jQuery(document).ready(function ($) {
		$(".moretext").shorten({
	showChars: 40,
	moreText: "<div class='viewtoggle'>More</div>",
	lessText: "<div><div class='viewtoggle less'>Less</div></div>"
});
		});
</script> -->
<style type="text/css">
  .fill-height-or-more {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
  -ms-flex-direction: column;
  flex-direction: row;
  -ms-flex-pack: distribute;
      justify-content: space-around;
  min-height: calc(100% - 60px);
  -ms-flex-wrap: wrap;
      flex-wrap: wrap;
}
.fill-height-or-more > div {
/*
-webkit-box-flex: 1;
  -ms-flex: 1;
  flex: 1;
*/
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: center;
  -ms-flex-pack: center;
  justify-content: center;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
  -ms-flex-direction: column;
  flex-direction: column;
  -webkit-box-pack: space-evenly;
  -ms-flex-pack: space-evenly;
  justify-content: space-evenly;
  /* height: auto; */
  /* margin-top: auto; */
  /* margin-bottom: auto; */
  /* margin: 0px 0px; */
  padding: 10px;
  /* align-items: flex-start;
  align-self: flex-start; */
}
p.header-title {
  text-align: center;
  padding-left: 0;
  padding-right: 0px;
}
.text-content {
   margin: 20px 0px;
}
.fullheightarea2 {
  min-height: 100vh;
  height: 100%;
    background-size: 100%;
  background-repeat: no-repeat;
}
.popup .book-room-now {
  margin-bottom: 30px;
  text-align: center;
}
.popup .offer-feature a.button {
  background-color: hsl(59, 91%, 51%);
  color: black;
  width: auto;
  display: inline-block;
  /* margin: 0 auto; */
  padding: 10px 30px;
}
.span4 img.alignnone {
  /* position: relative; */
  display: inline;
  /* float: right; */
    margin-bottom: -10px;
  margin-left: 10px;
}
.popup .text-content p {
  margin-bottom: 5%;
  text-align: left;
    margin-top: 5%;
}
.popup ul {
  text-align: left;
  /* list-style: circle; */
  margin-left: 15px;
  list-style-type: disc;
  margin-bottom: 30px;
}
.popup .text-content {
  margin: 0px 5vw;
}
.text-content h2 {
  margin: 1vw 0;
}
.mobilesmaller.fillwidth.aboutimage {
  background-size: auto 100%;
  background-position: center;
  -ms-flex-item-align: start;
      align-self: flex-start;
  justify-self: flex-start;
  vertical-align: top;
  padding-bottom: 100%;
}
.mobilesmaller.fillwidth.aboutimage::before {
    content: "";
    float: left;
    padding-top: 100%;
}
.aboutpage .header-title {
font-size: 2.4vw;
  /* margin-left: 10px; */
  /* bottom: 0px; */
  margin: auto;
  text-align: left;
  /* padding-left: 10px; */
  /* word-wrap: break-word; */
  width: 100%;
  line-height: 1em;
  margin-bottom: 8px;
  padding: 15px 0px;
  text-align: center;
}
.room-type_pop {
  font-size: 1.3em;
}
.titlepop h1, .titlepop h2 {
  font-size: 5.5em;
  font-weight: bold;
  /* font-family: "proxima_nova_bold"; */
  font-family: 'Phosphate-s',arial,sans-serif;
  letter-spacing: 2px;
  color: black;
  line-height: 1.1em;
}
.titlepop h2 {
  color: black;
  font-size: 1.5em;
  font-weight: 100;
  font-family: proxima_nova_bold-con;
}
.titlepop {
  padding: 3vw 0px;
}
.aboutpage p {
  /* font-size: 1.6em; */
  text-transform: unset;
  color: black;
}
.teamtext {
  padding-top: 15px;
  margin-top: 0px;
}
.viewtoggle {
  font-size: .7em;
  /* font-weight: bold; */
}
/*
.roomspage {
  padding-right: 20px;
  padding-left: 20px;
}
*/
.moreholder {
  text-align: center;
}
a.morelink {
  display: block;
}
.popup.people.aboutpage {
  padding-left: 3vw;
  padding-right: 3vw;
}
.bottomsection h2 {
    font-size: 9em;
  font-family: 'Phosphate-in',arial,sans-serif;
  line-height: 1em;
  text-align: center;
  color: #f9ee50;
  font-weight: 300;
}
.bottomsection a {
    font-weight: 700;
  font-family: "proxima_nova_bold",helvetica,ariel,sans-serif;
  letter-spacing: .03em;
    font-size: 2em;
    color: black;
}
.normalbold {
  font-size: 1.4em;
  font-weight: 700;
  line-height: 1.2em;
  text-transform: uppercase;
  font-family: "proxima_nova_bold",arial,sans-serif;
    font-size: 2em;
}
.Aligner.nopadding {
  min-height: initial;
}
.titlepop:first-child {
  padding-bottom: 0px;
}
@media (max-width: 768px) {
.fill-height-or-more > div {
    display: block;
  -ms-flex-wrap: wrap;
      flex-wrap: wrap;
  -ms-flex-preferred-size: 100%;
      flex-basis: 100%;
}
  .fill-height-or-more {
  -ms-flex-wrap: wrap;
      flex-wrap: wrap;
        display: block;
}
.text-content {
  text-align: left;
      margin-top: 50px;
}
.popup .text-content {
    padding-top: 50px;
}
.popup.people.aboutpage {
  padding-left: 3vw;
  padding-right: 3vw;
  font-size: .9em;
    margin-bottom: 180px;
}
.bottomsection h2 {
  font-size: 5em;
  padding-bottom: 15px;
  }
.titlepop {
  padding: 3em 0px;
  font-size: .9em;
  text-align: center;
}
}
@media (max-width: 992px) {
  .aboutpage .header-title {
      font-size: 1.4em !important;
  }
}
.col-xs-5ths,
.col-sm-5ths,
.col-md-5ths,
.col-lg-5ths {
    position: relative;
    min-height: 1px;
    padding-right: 15px;
    padding-left: 15px;
}
.col-xs-5ths {
    width: 20%;
    float: left;
}
@media (min-width: 768px) {
    .col-sm-5ths {
        width: 20%;
        float: left;
    }
}
@media (min-width: 992px) {
    .col-md-5ths {
        width: 20%;
        float: left;
    }
}
@media (min-width: 1200px) {
    .col-lg-5ths {
        width: 20%;
        float: left;
    }
}
</style>
<?php if (have_posts()) : while (have_posts()) : the_post();?>
  <div id="headerwrap">
    <div class="booknow">
      <img src="<?php echo get_template_directory_uri(); ?>/img/bookhere.gif" alt="book here" title="book">
    </div>
  </div>
  <div class="popup stayfree people aboutpage" role="main">
<div class="container-fluid center-block text-center">
	<div class="row">
	    <div class="col-xs-12 col-md-8 col-md-offset-2 ">
	       <div class="titlepop">
		        <h1><?php the_title();?></h1>
	        <h2 style="color: black;"><?php the_field('sub_title'); ?></h2>
	       </div>
	        <p class=" titlepop"><?php echo get_the_content();?></p>
	    </div>
	</div><!--  // SHAPELESS BECOMING -->
</div>
<div class="container-fluid roomspage some-area Aligner nopadding" >
    <div class="row fill-height-or-more smallbox">
      <?php while(has_sub_field('articles')): ?>
	    <div class="col-xs-12 col-sm-5ths col-md-5ths">
		    <div class="mobilesmaller fillwidth aboutimage" style="background-image: url(<?php the_sub_field('image'); ?>);">
		    </div>
		      <p class="header-title"><?php the_sub_field('title'); ?></p>
	    </div>
      <?php endwhile; ?>
	</div>
</div>
<div class="container-fluid titlepop center-block text-center bottomsection">
	<div class="row">
	    <div class="col-xs-12 ">
	        <h2><?php the_field('contact_us'); ?></h2>
	        <a class="" target="_top" href="mailto:<?php the_field('contact_us_email'); ?>?Subject=Amstel%20House%20Page:%20Stay%20free"><?php the_field('contact_us_email'); ?></a>
	    </div>
	</div>
</div>
</div>
<?php endwhile; endif; ?>
<?php get_footer(); ?>
