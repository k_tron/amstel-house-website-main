<?php
/*
Template Name: Gallery Page
*/

get_header(); ?>


<?php while ( have_posts() ) : the_post(); ?>


<div id="headerwrap">
<div class="booknow">
				<img src="<?php echo get_template_directory_uri(); ?>/img/bookhere.gif" alt="book here" title="book">
			</div>
<?php $thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "full" );
	     ?>
	    <div class="mobilesmaller fillwidth fullheightarea" style="background-image: url('<?php echo $thumbnail[0]; ?>');">
	    </div>

<h1 class="header-title"><?php the_title(); ?></h1>
<?php
					$link_f = get_field( 'main_image_link', 'option' );
					if ( $link_f ) {
						$link = $link_f;
					} else {
						$link = '/best-price-guarantee';
					}
 ?>
   <a href="<?php echo esc_url( $link ); ?>">
    <div class="hidden-xs">
        <p class="hostel-adrress"><?php the_field('line_below_slider', 'option'); ?></p>
    </div>

    <div class="visible-xs-block">
        <p class="hostel-adrress"><?php the_field('mobile_line_below_slider', 'option'); ?></p>
    </div>
  </a>	</div>

	<div id="main" class="location" role="main">
		<div class="container fullheightarea Aligner hidden-xs">
		<div class="row">
			<div class="col-xs-12 col-md-10 col-md-offset-1">
				<div class="welcome">
					<div class="welcome-body span4 collapse-group">
						<div class="intro">
							<?php the_content(); ?>


    					</div>

					</div>
				</div>
			</div>
		</div>
	</div>



<section id="servicespagea">
	<div class="gallery">



                <div class="Grid">

                      <?php
    $gallery_ary = array();
    $gallery_no = 1;

    while(has_sub_field('gallery_list')):

      // Collect images into array
      $gallery_ary[ $gallery_no ] = get_sub_field('gallery_images');
    ?>

                    <div class=" Grid-item three odder">
	                    <a href="#" class="new-gallery badgec left" data-gallery-id="<?php echo $gallery_no; ?>"  style="background-image: url('<?php the_sub_field('gallery_cover'); ?>');">


                        <div class="hiddenholder">
	                        <h3><?php the_sub_field('gallery_title'); ?></h3>
                        </div>

	                    </a>
                    </div><?php $gallery_no++; endwhile; ?>
                </div>

            <div class="Grid">
	            <div class="full-widthimage vcenterparent" style=" width: 100%" >


		            <a target="_blank" href="https://instagram.com/amstelhouse/" style="display: block; position: relative;">
									<div class="fullhighervcenter zindextop">
															 <div class="vcentertextabs">
								 <?php the_field('instagram_text'); ?>
								 </div>
							 </div>
			               <?php echo do_shortcode('[instagram-feed id="1148852576" accesstoken="1148852576.3a81a9f.d76d62b4f6384c82a6ca67869ad5ed0f"]') ?>


		            </a>

            </div>
            </div>

</section>
<!-- #main -->
	</div>
	 <script type="text/javascript">
      jQuery(document).ready(function() {
        var thisgallery = [];
        <?php
          // Loop JS for Gallery
          foreach ($gallery_ary as $gallery_key => $this_gallery) { ?>
            thisgallery[ <?php echo $gallery_key; ?> ] = [
              <?php foreach($this_gallery as $this_gallerydata) {
                // Loop each image in gallery ?>
                {
                  href : "<?php echo $this_gallerydata["url"]; ?>",

                },
              <?php } // End loop $this_gallery ?>
            ];
        <?php } // End loop $gallery_ary


				 ?>

        // Click to open gallery
jQuery('.new-gallery').on('click', function() {
var newgal_id = jQuery(this).attr('data-gallery-id');
jQuery.fancybox.open(thisgallery[ newgal_id ], {
padding: 0,
margin: 0,
autoSize: true,
autoResize: true,
tpl: {
closeBtn: '<i class="material-icons closebox">close</i>'
},
afterShow  : function () {
jQuery('.fancybox-wrap').touchwipe({
wipeLeft: function() { jQuery.fancybox.next(); },
wipeRight: function() { jQuery.fancybox.prev() },
min_move_x: 20,
min_move_y: 20,
preventDefaultEvents: true
});
},
beforeShow: function(){
jQuery("body").css({'overflow-y':'hidden'});
},
afterClose: function(){
jQuery("body").css({'overflow-y':'visible'});
},
fitToView: true,
openEffect: 'none',
openMethod: "changeIn",
openSpeed: 0,
autoCenter: true,
mouseWheel: false
});
return false;
});
      });
    </script>

<?php
endwhile;
get_footer();
