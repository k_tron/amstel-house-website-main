<?php
/*
Template Name: Rooms Page
*/

get_header(); ?>
<style type="text/css">
.fill-height-or-more {
	display: -webkit-box;
	display: -ms-flexbox;
	display: flex;
	-webkit-box-orient: vertical;
	-webkit-box-direction: normal;
	-webkit-flex-direction: column;
	-moz-box-orient: vertical;
	-moz-box-direction: normal;
	-ms-flex-direction: column;
	/* flex-direction: row; */
	-ms-flex-pack: distribute;
	justify-content: space-around;
	min-height: calc(100% - 60px) ;
}
.fill-height-or-more > div {
	-webkit-box-flex: 1;
	-ms-flex: 1;
	flex: 1;
	display: -webkit-box;
	display: -ms-flexbox;
	display: flex;
	-webkit-box-pack: center;
	-ms-flex-pack: center;
	justify-content: center;
	-webkit-box-orient: vertical;
	-webkit-box-direction: normal;
	-ms-flex-direction: column;
	flex-direction: column;
	-webkit-box-pack: space-evenly;
	-ms-flex-pack: space-evenly;
	justify-content: space-evenly;
	/* height: auto; */
	margin-top: auto;
	margin-bottom: auto;
}
.text-content {
	margin: 20px 0px;
}
.fullheightarea2 {
	min-height: 100vh;
	height: 100%;
	background-size: 100%;
	background-repeat: no-repeat;
}
.popup .book-room-now {
	margin-bottom: 30px;
	text-align: center;
}
.popup .offer-feature a.button {
	background-color: hsl(59, 91%, 51%);
	color: black;
	width: auto;
	display: inline-block;
	/* margin: 0 auto; */
	padding: 10px 30px;
}
.popup .offer-feature h4 {
	display: none;
}
.span4 img.alignnone {
	/* position: relative; */
	display: inline;
	/* float: right; */
	margin-bottom: -10px;
	margin-left: 10px;
}
.popup .text-content p {
	margin-bottom: 5%;
	text-align: left;
	margin-top: 5%;
}
.popup ul {
	text-align: left;
	/* list-style: circle; */
	margin-left: 15px;
	list-style-type: disc;
	margin-bottom: 30px;
}
.popup .text-content {
	margin: 0px 5vw;
}
.text-content h2 {
	margin: 1vw 0;
}
@media (max-width: 768px) {
	.fillwidth.fullheightarea2 {
		/*  min-height: 67vh;*/
		min-height: 56vh;
	}
	.fill-height-or-more > div {
		display: block;
	}
	.fill-height-or-more {
		-ms-flex-wrap: wrap;
		flex-wrap: wrap;
		/* display: block; */
		/* flex-wrap: wrap-reverse; */
		-webkit-box-orient: vertical !important;
		-webkit-box-direction: reverse !important;
		-ms-flex-direction: column-reverse !important;
		flex-direction: column-reverse !important;
	}
	.text-content {
		text-align: left;
		margin-top: 50px;
	}
	.popup .text-content {
		padding-top: 50px;
		margin: 0px 5px;
	}
	.text-content h2 {
		text-align: center;
	}
}
</style>
<?php while ( have_posts() ) : the_post(); ?>
	<div id="headerwrap">
		<div class="booknow">
			<img src="<?php echo get_template_directory_uri(); ?>/img/bookhere.gif" alt="book here" title="book">
		</div>
		<?php
		$img_src = wp_get_attachment_image_url( get_post_thumbnail_id( $post->ID ), 'full' );
		$img_srcset = wp_get_attachment_image_srcset( get_post_thumbnail_id( $post->ID ), 'full' );
		$img_alt = get_post_meta( get_post_thumbnail_id( $post->ID ), '_wp_attachment_image_alt', true);
		?>
		<img class=" mobilesmaller fillwidth fullheightarea" src="<?php echo esc_url( $img_src ); ?>"
		srcset="<?php echo esc_attr( $img_srcset ); ?>"
		sizes="(max-width: 1024px) 100vw, 1024w" alt="<?php echo $img_alt; ?>">
		<div class="header-title"><?php the_title(); ?></div>
		<?php
		$link_f = get_field( 'main_image_link', 'option' );
		if ( $link_f ) {
			$link = $link_f;
		} else {
			$link = '/best-price-guarantee';
		}
		?>	 <a href="<?php echo esc_url( $link ); ?>">
			<div class="hidden-xs">
				<p class="hostel-adrress"><?php the_field('line_below_slider', 'option'); ?></p>
			</div>

			<div class="visible-xs-block">
				<p class="hostel-adrress"><?php the_field('mobile_line_below_slider', 'option'); ?></p>
			</div></a>
		</div>

		<div id="main" class="location" role="main">
			<div class="mobilesmaller container-fluid fullheightarea Aligner">
				<div class="row">
					<div class="col-xs-12 col-md-10 col-md-offset-1">
						<div class="welcome">
							<div class="welcome-body span4 collapse-group">
								<div class="intro">
									<?php the_content(); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<section id="roomsrow">
				<div class="container-fluid se-container roomspage">
					<div class="row">
						<div class="col-xs-12 col-md-10 col-md-offset-1">

							<div class="row evenheight-remove">
								<?php
								$rooms = new WP_Query(array('post_type'=> 'rooms', 'order' => 'ASC', 'orderby' => 'title') );
								?>
								<?php while ( $rooms->have_posts() ) : ?>
									<?php $rooms->the_post(); ?>
									<?php $room = get_the_ID(); ?>
									<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 mb-30">
										<div class="two-deg smallblog card border-primary h-100 bg-light roomspage2 ">
											<a href="<?php the_permalink(); ?>">
												<div class="square d-flex align-items-center justifty-content-center">
													<?php the_post_thumbnail('medium_large'); ?>
													<div class="hiddenholder text-shadow-2">
														<div class="more">
															<?php the_field('text_more', 'option'); ?>
														</div>
													</div>
												</div>
												</a>
												<div class="card-body ">
													<header class="entry-header room-type text-center">
														<h3 class=' display-3 phosphate-h1'>
															<?php the_title(); ?>
															<em><?php the_field('room_subtitle'); ?></em>
														</h3>
													</header>

												</div>
											</div>
										</div>
									<?php endwhile;
									wp_reset_postdata();
									?>
								</div>

							</div>
						</div>
					</section>
					<!-- #main -->
				</div>

				<?php
			endwhile;
			get_footer();
