<?php
/*
Template Name: Offer - Subpage
*/
get_header(); ?>

<style type="text/css">
.fill-height-or-more {
	display: -webkit-box;
	display: -ms-flexbox;
	display: flex;
	-webkit-box-orient: vertical;
	-webkit-box-direction: normal;
	-webkit-flex-direction: column;
	-moz-box-orient: vertical;
	-moz-box-direction: normal;
	-ms-flex-direction: column;
	flex-direction: row;
	-ms-flex-pack: distribute;
	justify-content: space-around;
  height: calc(100vh - 90px);
}
.fill-height-or-more > div {
	-webkit-box-flex: 1;
	-ms-flex: 1;
	flex: 1;
	display: -webkit-box;
	display: -ms-flexbox;
	display: flex;
	-webkit-box-pack: center;
	-ms-flex-pack: center;
	justify-content: center;
	-webkit-box-orient: vertical;
	-webkit-box-direction: normal;
	-ms-flex-direction: column;
	flex-direction: column;
	-webkit-box-pack: space-evenly;
	-ms-flex-pack: space-evenly;
	justify-content: space-evenly;
	/* height: auto; */
	margin-top: auto;
	margin-bottom: auto;
}
.fill-height-or-more > div > img {
	object-fit: cover;

	width: 100%;
}
.text-content {
	margin: 20px 0px;
}
.fullheightarea2 {
	min-height: 100vh;
	height: 100%;
	background-size: 100%;
	background-repeat: no-repeat;
}
.popup .book-room-now {
	margin-bottom: 30px;
	text-align: center;
}
.popup .offer-feature a.button {
	background-color: hsl(59, 91%, 51%);
	color: black;
	width: auto;
	display: inline-block;
	/* margin: 0 auto; */
	padding: 10px 30px;
}
.popup .offer-feature h4 {
	display: none;
}
.span4 img.alignnone {
	/* position: relative; */
	display: inline;
	/* float: right; */
	margin-bottom: -10px;
	margin-left: 10px;
}
.popup .text-content p {
	margin-bottom: 5%;
	text-align: left;
	margin-top: 5%;
}
.popup ul {
	text-align: left;
	/* list-style: circle; */
	margin-left: 15px;
	list-style-type: disc;
	margin-bottom: 30px;
}
.popup .text-content {
	margin: 0px 5vw;
}
.text-content h2 {
	margin: 1vw 0;
}
@media (max-width: 768px) {
	.fillwidth.fullheightarea2 {
		/*  min-height: 67vh;*/
		min-height: 56vh;
	}
	.fill-height-or-more > div {
		display: block;
	}
	.fill-height-or-more {
		-ms-flex-wrap: wrap;
		flex-wrap: wrap;
		/* display: block; */
		/* flex-wrap: wrap-reverse; */
		-webkit-box-orient: vertical !important;
		-webkit-box-direction: reverse !important;
		-ms-flex-direction: column-reverse !important;
		flex-direction: column-reverse !important;
		height: initial;
	}
	.text-content {
		text-align: left;
		margin-top: 50px;
	}
	.popup .text-content {
		padding-top: 25px;
		margin: 0px 5px;
	}
	.text-content h2 {
		text-align: center;
	}
}
</style>
<?php if (have_posts()) : while (have_posts()) : the_post();
//$options_words_id = get_options_page_id('acf-options-ah-common-words');
?>
	<div id="headerwrap">
		<div class="booknow">
			<img src="<?php echo get_template_directory_uri(); ?>/img/bookhere.gif" alt="book here" title="book">
		</div>
	</div>
	<div class="popup people" role="main">
		<section class="roomspage2 offer-feature">
			<div class="container-fluid">
				<div class="row some-area fill-height-or-more" style="  flex-direction: row;">
					<?php
                    $room_i = 1;
                    while (has_sub_field('offer_features')):
                        ?>
						<div class="col-xs-12 col-md-6">
							<a class="btn blackboxa btoo button" href="<?php echo get_permalink($post->post_parent); ?>#offers"><div ><i class="fas fa-arrow-left"></i> Offers</div></a>
							<div class="text-content fill-height-or-more">
								<div class="welcome-body span4 collapse-group">
									<div class="">
										<h1><?php the_title(); ?></h1>
										<?php the_content(); ?>
											<div class="book-room-now">
												<a class="button getattr" href="#"><?php the_field('text_book_now', 'option'); ?></a>

											</div>
									</div>

								</div>
							</div>
						</div>
						<div class="col-xs-12 col-md-6 nopadding">
								<?php $imagea = get_sub_field('feature_image');
                            if (!empty($imagea)) {
                                ?>
								<!-- <div class=" fillwidth fullheightarea2 imgarray" style="background-image: url( <?php echo $imagea['url']; ?>);"></div> -->
								<img src="<?php echo $imagea['url']; ?>">
							<?php
                            } else {	?>

								<!-- <div class=" fillwidth fullheightarea2" style="background-image: url( <?php the_sub_field('feature_image'); ?>);"></div> -->
								<img src="<?php the_sub_field('feature_image'); ?>">
						</div>
								<?php
                            }
                         $room_i++; endwhile; ?>

						</div>
					</div>
				</section>
			</div>
		<?php endwhile; endif; ?>
		<?php get_footer(); ?>
