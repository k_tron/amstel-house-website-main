<?php

/**
* The template for displaying all single posts.
*
* @package Amstelhouse
*/
get_header();

?>
<div></div>
<style type="text/css">
.fill-height-or-more {
	display: -webkit-box;
	display: -ms-flexbox;
	display: flex;
	-webkit-box-orient: vertical;
	-webkit-box-direction: normal;
	-webkit-flex-direction: column;
	-moz-box-orient: vertical;
	-moz-box-direction: normal;
	-ms-flex-direction: column;
	flex-direction: row;
	-ms-flex-pack: distribute;
	justify-content: space-around;
	/* min-height: calc(100vh - 90px); */
	height: calc(100vh - 90px);
}
.fill-height-or-more > div {
	-webkit-box-flex: 1;
	-ms-flex: 1;
	flex: 1;
	display: -webkit-box;
	display: -ms-flexbox;
	display: flex;
	-webkit-box-pack: center;
	-ms-flex-pack: center;
	justify-content: center;
	-webkit-box-orient: vertical;
	-webkit-box-direction: normal;
	-ms-flex-direction: column;
	flex-direction: column;
	-webkit-box-pack: space-evenly;
	-ms-flex-pack: space-evenly;
	justify-content: space-evenly;
	/* height: auto; */
	margin-top: auto;
	margin-bottom: auto;
}
.text-content {
	margin: 20px 0px;
}
.fullheightarea2 {
	/* min-height: 100vh; */
	height: 100%;
	background-size: 100%;
	background-repeat: no-repeat;
}
.popup .book-room-now {
	margin-bottom: 30px;
	text-align: center;
}
.popup .offer-feature a.button {
	background-color: hsl(59, 91%, 51%);
	color: black;
	width: auto;
	display: inline-block;
	/* margin: 0 auto; */
	padding: 10px 30px;
}
.popup .offer-feature h4 {
	display: none;
}
.span4 img.alignnone {
	/* position: relative; */
	display: inline;
	/* float: right; */
	margin-bottom: -10px;
	margin-left: 10px;
}
.popup .text-content p {
	margin-bottom: 5%;
	text-align: left;
	margin-top: 5%;
}
.popup ul {
	text-align: left;
	/* list-style: circle; */
	margin-left: 15px;
	list-style-type: disc;
	margin-bottom: 30px;
	columns: 2;
 -webkit-columns: 2;
 -moz-columns: 2;
}
.popup .text-content {
	margin: 0px 5vw;
}
.text-content h2 {
	margin: 1vw 0;
}
@media (min-width:769px) {
	.rs-default-template2 {
		height: 100%;
	}
}
@media (max-width: 768px) {
	.fillwidth.fullheightarea2 {
		/*  min-height: 67vh;*/
		/* min-height: 56vh; */
	}
	.fill-height-or-more > div {
		display: block;
	}
	.fill-height-or-more {
		-ms-flex-wrap: wrap;
		flex-wrap: wrap;
		display: flex;
		/* flex-wrap: wrap-reverse; */
		-webkit-box-orient: vertical !important;
		-webkit-box-direction: reverse !important;
		-ms-flex-direction: column-reverse !important;
		flex-direction: column-reverse !important;
		padding-bottom: 25px;
	 height: 100%;
	}
	.rs-default-template2 {
	  height: 100vw;
	}
	.text-content {
		text-align: left;
		margin-top: 50px;
	}
	.popup .text-content {
		padding-top: 50px;
		margin: 0px 5px;
		text-align: center;
		  padding-bottom: 50px;
			  height: initial !important;
	}
	.text-content h2 {
		text-align: center;
	}

}
div#mini-carousel {
	align-items: center;
	/* width: 100%; */
}
.fillwidth.fullheightarea2 {
	display: block;
	width: 100%;
}
a.new-gallery.badgec.left {
	/* display: flex;
	align-items: center;
	justify-items: center;
	object-fit: fill; */
}
a.new-gallery img {
	object-fit: cover;
}
</style>
<div id="headerwrap">
	<div class="booknow">
		<img src="<?php echo get_template_directory_uri(); ?>/img/bookhere.gif" alt="book here" title="book">
	</div>
</div>
<div id="primary" class="content-area">
	<main id="main" class="site-main popup people roomsingle" role="main">
		<section class="roomspage2 offer-feature">
			<div class="container-fluid">
				<div class="row some-area fill-height-or-more" style="  flex-direction: row;">
					<?php while ( have_posts() ) : the_post(); ?>
						<div class="col-xs-12 col-md-6">
							<a class="btn blackboxa btoo button bbbb" href="<?php echo $_SERVER['HTTP_REFERER'];?>#roomsrow"><div ><i class="fas fa-arrow-left"></i> Rooms</div></a>
							<div class="text-content fill-height-or-more">
								<div class="welcome-body span4 collapse-group">
									<div class="room-type">
										<h1><?php the_title(); ?></h1>
										<h3 class="text-center"><?php the_field('room_subtitle'); ?></h2>

											<?php // the_field( 'text_personper', 'option'); the_field( 'number_people'); ?>
											<?php the_field('room_detail'); ?>
											<div class="mb-30 iconrow">
												<?php

if( have_rows('iconrep') ):

    while ( have_rows('iconrep') ) : the_row();

?>
					<img class="image-responsive" style="max-height: 48px; padding: 5px;" src="<?php the_sub_field('icon'); ?>" alt="<?php the_field('room_title'); ?>">
<?php
    endwhile;

else :

    // no rows found
		?>
		<img class="image-responsive" src="<?php echo get_template_directory_uri() . '/img/rooms/' . get_field( 'number_people') . '-icon.png'; ?>" alt="<?php the_field('room_title'); ?>">

		<?php

endif;

?>
											</div>
											<h4><?php the_field('price'); ?> </h4>
										</div>

										<div class="book-room-nowa">
											<a class="button groupselect getattr" href="#booknow" alt="book now"><?php the_field( 'text_book_now', 'option' ); ?></a>
										</div>
									</div>
								</div>
							</div>

						<div class="col-xs-12 col-md-6 nopadding">

							<div class=" fillwidth fullheightarea2" >
								<?php
								$images = get_field('images');
								if( $images ):
									?>
									<div class="new-gallery badgec left">
										<div class="royalSlider new-royalslider-6 rsDefault rs-default-template2 rsHor" style="  width: 100%;
										">
										<!-- simple image -->
										<?php foreach( $images as $image ):
											$img_srcset = wp_get_attachment_image_srcset($image['ID']);
											?>
											<!-- <img class="rsImg" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" srcset="<?php echo $img_srcset; ?>" data-alt="<?php echo $image['title'];?>" /> -->
											<a class="rsImg" href="<?php echo $image['url']; ?>"><?php echo $image['alt']; ?></a>

										<?php endforeach; ?>
									</div>
								</div>
							<?php endif; ?>
						</div>
					</div>


					<?php endwhile; // end of the loop. ?>
				</div>
			</div>
		</section>
	</main><!-- #main -->
</div><!-- #primary -->

<?php get_footer();
 ?>

 <script type="text/javascript">
 $(document).ready(function() {
  $(".royalSlider").royalSlider(
 	 {template:'default',image_generation:{lazyLoading:!0,imageWidth:'100%',imageHeight:'100%',thumbImageWidth:'',thumbImageHeight:''},thumbs:{thumbWidth:96,thumbHeight:72},autoPlay:{delay:5000},fullscreen:{nativeFS:!0},video:{forceMaxVideoCoverResolution:'standard'},block:{fadeEffect:!1,moveOffset:20,speed:400,delay:200},width:'100%',height:'calc(100vh + 12vw)',slidesSpacing:0,imageScaleMode:'fill',imageScalePadding:0,arrowsNavHideOnTouch:!0,loop:!0,numImagesToPreload:0,sliderDrag:1,navigateByClick:1,fadeinLoadedSlide:!1}
  );
 });
 </script>
